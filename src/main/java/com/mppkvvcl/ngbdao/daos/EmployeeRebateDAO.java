package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.EmployeeRebateDAOInterface;
import com.mppkvvcl.ngbdao.repositories.EmployeeRebateRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.EmployeeRebateInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
public class EmployeeRebateDAO implements EmployeeRebateDAOInterface<EmployeeRebateInterface>{

    private Logger logger = GlobalResources.getLogger(EmployeeRebateDAO.class);

    @Autowired
    private EmployeeRebateRepository employeeRebateRepository;

    @Override
    public EmployeeRebateInterface getByCodeAndDate(String code, Date date) {
        final String methodName = "getByCodeAndDate() : ";
        logger.info(methodName + "called");
        EmployeeRebateInterface employeeRebateInterface = null;
        if(code != null && date != null){
            employeeRebateInterface = employeeRebateRepository.findByCodeAndDate(code,date);
        }
        return employeeRebateInterface;
    }

    @Override
    public EmployeeRebateInterface add(EmployeeRebateInterface employeeRebateInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        EmployeeRebateInterface addedEmployeeRebateInterface = null;
        if(employeeRebateInterface != null){
            addedEmployeeRebateInterface = employeeRebateRepository.save(employeeRebateInterface);
        }
        return addedEmployeeRebateInterface;
    }

    @Override
    public EmployeeRebateInterface update(EmployeeRebateInterface employeeRebateInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        EmployeeRebateInterface updatedEmployeeRebateInterface = null;
        if(employeeRebateInterface != null){
            updatedEmployeeRebateInterface = employeeRebateRepository.save(employeeRebateInterface);
        }
        return updatedEmployeeRebateInterface;
    }

    @Override
    public EmployeeRebateInterface get(EmployeeRebateInterface employeeRebateInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        EmployeeRebateInterface fetchedEmployeeRebateInterface = null;
        if(employeeRebateInterface != null){
            fetchedEmployeeRebateInterface = employeeRebateRepository.findOne(employeeRebateInterface.getId());
        }
        return fetchedEmployeeRebateInterface;

    }

    @Override
    public EmployeeRebateInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        EmployeeRebateInterface fetchedEmployeeRebateInterface = null;
        fetchedEmployeeRebateInterface = employeeRebateRepository.findOne(id);
        return fetchedEmployeeRebateInterface;

    }
}
