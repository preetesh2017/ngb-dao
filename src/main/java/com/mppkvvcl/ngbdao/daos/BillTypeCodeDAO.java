package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.BillTypeCodeDAOInterface;
import com.mppkvvcl.ngbdao.repositories.BillTypeCodeRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.BillTypeCodeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BillTypeCodeDAO implements BillTypeCodeDAOInterface<BillTypeCodeInterface> {

    private Logger logger = GlobalResources.getLogger(BillTypeCodeDAO.class);

    @Autowired
    private BillTypeCodeRepository billTypeCodeRepository;

    @Override
    public BillTypeCodeInterface add(BillTypeCodeInterface billTypeCodeInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        BillTypeCodeInterface addedBillTypeCodeInterface = null;
        if(billTypeCodeInterface != null){
            addedBillTypeCodeInterface = billTypeCodeRepository.save(billTypeCodeInterface);
        }
        return addedBillTypeCodeInterface;
    }

    @Override
    public BillTypeCodeInterface update(BillTypeCodeInterface billTypeCodeInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        BillTypeCodeInterface updatedBillTypeCodeInterface = null;
        if(billTypeCodeInterface != null){
            updatedBillTypeCodeInterface = billTypeCodeRepository.save(billTypeCodeInterface);
        }
        return updatedBillTypeCodeInterface;
    }

    @Override
    public BillTypeCodeInterface get(BillTypeCodeInterface billTypeCodeInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        BillTypeCodeInterface fetchedBillTypeCodeInterface = null;
        if(billTypeCodeInterface != null){
            fetchedBillTypeCodeInterface = billTypeCodeRepository.findOne(billTypeCodeInterface.getId());
        }
        return fetchedBillTypeCodeInterface;
    }

    @Override
    public BillTypeCodeInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        BillTypeCodeInterface billTypeCodeInterface = null;
        billTypeCodeInterface = billTypeCodeRepository.findOne(id);
        return billTypeCodeInterface;
    }

    @Override
    public BillTypeCodeInterface getByCode(String code) {
        final String methodName = "getByCode() : ";
        logger.info(methodName + "called");
        BillTypeCodeInterface billTypeCodeInterface = null;
        if(code != null){
            billTypeCodeInterface = billTypeCodeRepository.findByCode(code);
        }
        return billTypeCodeInterface;
    }
}
