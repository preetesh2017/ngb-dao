package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.PaymentDAOInterface;
import com.mppkvvcl.ngbdao.repositories.PaymentRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.Payment;
import com.mppkvvcl.ngbinterface.interfaces.PaymentInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
@Service
public class PaymentDAO implements PaymentDAOInterface<PaymentInterface> {
    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(PaymentDAO.class);

    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public long getCountByConsumerNo(String consumerNo) {
        final String methodName = "getCountByConsumerNo() : ";
        logger.info(methodName + "called");
        long count = -1;
        if(consumerNo != null){
            count = paymentRepository.countByConsumerNo(consumerNo);
        }
        return count;
    }

    @Override
    public List<PaymentInterface> getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        List<PaymentInterface> payments = null;
        if(consumerNo != null){
            payments = paymentRepository.findByConsumerNo(consumerNo);
        }
        return payments;
    }

    @Override
    public List<PaymentInterface> getByConsumerNo(String consumerNo, Pageable pageable) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called with pageable");
        List<PaymentInterface> payments = null;
        if(consumerNo != null && pageable != null){
            payments = paymentRepository.findByConsumerNo(consumerNo,pageable);
        }
        return payments;
    }

    @Override
    public List<PaymentInterface> getByConsumerNoOrderByPostingBillMonthAscendingWithPageable(String consumerNo, Pageable pageable) {
        final String methodName = "getByConsumerNoOrderByPostingBillMonthAscendingWithPageable() : ";
        logger.info(methodName + "called");
        List<PaymentInterface> payments = null;
        if(consumerNo != null && pageable != null){
            payments = paymentRepository.findByConsumerNoOrderByPostingBillMonthAscendingWithPageable(consumerNo,pageable);
        }
        return payments;
    }

    @Override
    public List<PaymentInterface> getByConsumerNoOrderByPostingBillMonthDescendingWithPageable(String consumerNo, Pageable pageable) {
        final String methodName = "getByConsumerNoOrderByPostingBillMonthDescendingWithPageable() : ";
        logger.info(methodName + "called");
        List<PaymentInterface> payments = null;
        if(consumerNo != null && pageable != null){
            payments = paymentRepository.findByConsumerNoOrderByPostingBillMonthDescendingWithPageable(consumerNo,pageable);
        }
        return payments;
    }

    @Override
    public List<PaymentInterface> getByConsumerNoAndPostingBillMonthAndDeletedAndPosted(String consumerNo, String billMonth, boolean deleted, boolean posted) {
        final String methodName = "getByConsumerNoAndPostingBillMonthAndDeletedAndPosted() : ";
        logger.info(methodName + "is called");
        List<PaymentInterface> payments = null;
        if(consumerNo != null && billMonth != null){
            payments = paymentRepository.findByConsumerNoAndPostingBillMonthAndDeletedAndPosted(consumerNo, billMonth, deleted, posted);
        }
        return payments;
    }

    @Override
    public List<PaymentInterface> getByConsumerNoAndDeletedAndPosted(String consumerNo, boolean deleted, boolean posted) {
        final String methodName = "getByConsumerNoAndDeletedAndPosted() : ";
        logger.info(methodName + "called");
        List<PaymentInterface> payments = null;
        if(consumerNo != null){
            payments = paymentRepository.findByConsumerNoAndDeletedAndPosted(consumerNo, deleted, posted);
        }
        return payments;
    }

    @Override
    public List<PaymentInterface> getByLocationCodeAndPostingBillMonth(String locationCode, String postingBillMonth) {
        final String methodName = "getByLocationCodeAndPostingBillMonth() : ";
        logger.info(methodName + "called");
        List<PaymentInterface> payments = null;
        if(locationCode != null && postingBillMonth != null){
            payments = paymentRepository.findByLocationCodeAndPostingBillMonth(locationCode, postingBillMonth);
        }
        return payments;
    }

    @Override
    public List<PaymentInterface> getByLocationCodeAndPayWindowAndPayDateAndDeleted(String locationCode, String payWindow, Date payDate, boolean deleted) {
        final String methodName = "getByLocationCodeAndPayWindowAndPayDateAndDeleted() : ";
        logger.info(methodName + "called");
        List<PaymentInterface> payments = null;
        if(locationCode != null && payDate != null && payWindow != null){
            payments = paymentRepository.findByLocationCodeAndPayWindowAndPayDateAndDeleted(locationCode, payWindow, payDate, deleted);
        }
        return payments;
    }

    @Override
    public PaymentInterface add(PaymentInterface payment) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        PaymentInterface insertedPayment = null;
        if(payment != null){
            insertedPayment = paymentRepository.save(payment);
        }
        return insertedPayment;
    }

    @Override
    public List<PaymentInterface> add(List<PaymentInterface> payments) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        List<PaymentInterface> insertedPayments = null;
        if(payments != null){
            insertedPayments = new ArrayList<>();
            for(PaymentInterface paymentInterface : payments){
                insertedPayments.add(paymentRepository.save(paymentInterface));
            }
        }
        return insertedPayments;
    }

    @Override
    public PaymentInterface update(PaymentInterface payment) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        PaymentInterface updatedPayment = null;
        if(payment != null){
            updatedPayment = paymentRepository.save(payment);
        }
        return updatedPayment;
    }

    @Override
    public PaymentInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called for id " + id);
        PaymentInterface payment = null;
        payment = paymentRepository.findOne(id);
        return payment;
    }

    @Override
    public PaymentInterface get(PaymentInterface payment) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        PaymentInterface existingPayment = null;
        if(payment != null){
            existingPayment = paymentRepository.findOne(payment.getId());
        }
        return existingPayment;
    }

    @Override
    public List<PaymentInterface> getByConsumerNoAndPayModes(final String consumerNo, final String[] payModes) {
        final String methodName = "getByConsumerNoAndPayModes() : ";
        logger.info(methodName + "called");
        List<PaymentInterface> payments = null;
        if (consumerNo != null && !consumerNo.isEmpty() && payModes != null && payModes.length > 0) {
            payments = paymentRepository.findByConsumerNoAndPayModeIn(consumerNo, payModes);
        }
        return payments;
    }

    @Override
    public List<PaymentInterface> getByConsumerNoAndDeleted(String consumerNo, boolean deleted) {
        final String methodName = "getByConsumerNoAndDeleted() : ";
        logger.info(methodName + "called");
        List<PaymentInterface> payments = null;
        if(consumerNo != null){
            payments = paymentRepository.findByConsumerNoAndDeleted(consumerNo,deleted);
        }
        return payments;
    }
}
