package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ConsumerGovernmentMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ConsumerGovernmentMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerGovernmentMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Created by SHIVANSHU on 15-09-2017.
 */
@Service
public class ConsumerGovernmentMappingDAO implements ConsumerGovernmentMappingDAOInterface<ConsumerGovernmentMappingInterface> {

    Logger logger = GlobalResources.getLogger(ConsumerGovernmentMappingDAO.class);

    @Autowired
    ConsumerGovernmentMappingRepository consumerGovernmentMappingRepository;

    @Override
    public ConsumerGovernmentMappingInterface add(ConsumerGovernmentMappingInterface consumerGovernmentMapping) {
        final String methodName = "add() :  ";
        logger.info(methodName+"starting to add");
        ConsumerGovernmentMappingInterface insertedConsumerGovernmentMapping = null;
        if (consumerGovernmentMapping != null){
            insertedConsumerGovernmentMapping = consumerGovernmentMappingRepository.save(consumerGovernmentMapping);
        }
        return insertedConsumerGovernmentMapping;
    }

    @Override
    public ConsumerGovernmentMappingInterface update(ConsumerGovernmentMappingInterface consumerGovernmentMapping) {
        final String methodName = "update() :  ";
        logger.info(methodName+"starting to update");
        ConsumerGovernmentMappingInterface newConsumerGovernmentMapping = null;
        if (consumerGovernmentMapping != null){
            newConsumerGovernmentMapping = consumerGovernmentMappingRepository.save(consumerGovernmentMapping);
        }
        return newConsumerGovernmentMapping;
    }

    @Override
    public ConsumerGovernmentMappingInterface get(ConsumerGovernmentMappingInterface consumerGovernmentMapping) {
        final String methodName = "get() :  ";
        logger.info(methodName+"starting to get");
        ConsumerGovernmentMappingInterface oldConsumerGovernmentMapping = null;
        if (consumerGovernmentMapping != null){
            oldConsumerGovernmentMapping = consumerGovernmentMappingRepository.findOne(consumerGovernmentMapping.getId());
        }
        return oldConsumerGovernmentMapping;
    }

    @Override
    public ConsumerGovernmentMappingInterface getById(long id) {
        final String methodName = "getById() :  ";
        logger.info(methodName+"starting to get by id");
        ConsumerGovernmentMappingInterface consumerGovernmentMapping = null;
        if (id != 0){
            consumerGovernmentMapping = consumerGovernmentMappingRepository.findOne(id);
        }
        return consumerGovernmentMapping;
    }

    @Override
    public ConsumerGovernmentMappingInterface getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() :  ";
        logger.info(methodName + "called");
        ConsumerGovernmentMappingInterface consumerGovernmentMapping = null;
        if (!StringUtils.isEmpty(consumerNo)) {
            consumerGovernmentMapping = consumerGovernmentMappingRepository.findByConsumerNo(consumerNo);
        }
        return consumerGovernmentMapping;
    }
}
