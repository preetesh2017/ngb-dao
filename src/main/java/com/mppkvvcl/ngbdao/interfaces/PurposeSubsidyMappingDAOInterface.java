package com.mppkvvcl.ngbdao.interfaces;
import java.util.Date;
import java.util.List;

public interface PurposeSubsidyMappingDAOInterface<T> extends DAOInterface<T> {
    public List<T> getBySubcategoryCodeAndPurposeOfInstallationIdAndEffectiveStartDateAndEffectiveEndDate(
            long subcategoryCode, long purposeOfInstallationId,Date date);

    public List<T> getBySubcategoryCodeAndEffectiveStartDateAndEffectiveEndDate
            (long subcategoryCode, Date date);
}
