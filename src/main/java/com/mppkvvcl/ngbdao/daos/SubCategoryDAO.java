package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.SubCategoryDAOInterface;
import com.mppkvvcl.ngbdao.repositories.SubCategoryRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.SubCategoryInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class SubCategoryDAO implements SubCategoryDAOInterface<SubCategoryInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(SubCategoryDAO.class);

    @Autowired
    private SubCategoryRepository subCategoryRepository ;

    @Override
    public SubCategoryInterface add(SubCategoryInterface subCategory) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        SubCategoryInterface insertedSubCategory = null;
        if (subCategory != null){
            insertedSubCategory = subCategoryRepository.save(subCategory);
        }
        return insertedSubCategory;
    }

    @Override
    public SubCategoryInterface update(SubCategoryInterface subCategory) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        SubCategoryInterface updatedSubCategory = null;
        if (subCategory != null){
            updatedSubCategory = subCategoryRepository.save(subCategory);
        }
        return updatedSubCategory;    }

    @Override
    public SubCategoryInterface get(SubCategoryInterface subCategory) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        SubCategoryInterface existingSubCategory = null;
        if (subCategory != null){
            existingSubCategory = subCategoryRepository.findOne(subCategory.getId());
        }
        return existingSubCategory;    }

    @Override
    public SubCategoryInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        SubCategoryInterface existingSubCategory = null;
        if(id != 0){
            existingSubCategory = subCategoryRepository.findOne(id);
        }
        return existingSubCategory;
    }

    @Override
    public List<SubCategoryInterface> getByTariffId(Long tariffId) {
        final String methodName = "getByTariffId() : ";
        logger.info(methodName + "called");
        List<SubCategoryInterface> fetchedSubCategory = null;
        if(tariffId != 0l){
            fetchedSubCategory = subCategoryRepository.findByTariffId(tariffId);
        }
        return fetchedSubCategory;
    }

    @Override
    public List<SubCategoryInterface> getByTariffIdAndPremiseType(Long tariffId, String premiseType) {
        final String methodName = "getByTariffIdAndPremiseType() : ";
        logger.info(methodName + "called");
        List<SubCategoryInterface> fetchedSubCategory = null;
        if(tariffId != 0l && premiseType != null){
            fetchedSubCategory = subCategoryRepository.findByTariffIdAndPremiseType(tariffId,premiseType);
        }
        return fetchedSubCategory;
    }

    @Override
    public List<SubCategoryInterface> getByTariffIdAndConnectedLoadAndPremiseType(long tariffId, BigDecimal connectedLoad, String premiseType) {
        final String methodName = "getByTariffIdAndConnectedLoadAndPremiseType() : ";
        logger.info(methodName + "called");
        List<SubCategoryInterface> fetchedSubCategory = null;
        if(tariffId != 0l && connectedLoad != null && premiseType != null){
            fetchedSubCategory = subCategoryRepository.findByTariffIdAndConnectedLoadAndPremiseType(tariffId , connectedLoad , premiseType);
        }
        return fetchedSubCategory;
    }

    @Override
    public List<SubCategoryInterface> getByTariffIdAndConnectedLoadAndPremiseTypeAndApplicantType(long tariffId, BigDecimal connectedLoad, String premiseType, String applicantType) {
        final String methodName = "getByTariffIdAndConnectedLoadAndPremiseTypeAndApplicantType() : ";
        logger.info(methodName + "called");
        List<SubCategoryInterface> fetchedSubCategory = null;
        if(tariffId != 0l && connectedLoad != null && premiseType != null && applicantType != null){
            fetchedSubCategory = subCategoryRepository.findByTariffIdAndConnectedLoadAndPremiseTypeAndApplicantType(tariffId , connectedLoad , premiseType, applicantType);
        }
        return fetchedSubCategory;
    }

    @Override
    public List<SubCategoryInterface> getByTariffIdAndConnectedLoadAndContractDemandAndPremiseTypeAndApplicantType(long tariffId, BigDecimal connectedLoad, BigDecimal contractDemand, String premiseType,String applicantType) {
        final String methodName = "getByTariffIdAndConnectedLoadAndContractDemandAndPremiseType() : ";
        logger.info(methodName + "called");
        List<SubCategoryInterface> fetchedSubCategory = null;
        if(tariffId != 0l && connectedLoad != null && premiseType != null && contractDemand != null){
            logger.info(methodName+"tariffId:"+tariffId+"connected Load:"+connectedLoad+"contract demand:"+contractDemand+"premise type:"+premiseType+"applicant type:"+applicantType);
            fetchedSubCategory = subCategoryRepository.findByTariffIdAndConnectedLoadAndContractDemandAndPremiseTypeAndApplicantType(tariffId , connectedLoad , contractDemand, premiseType,applicantType);
        }
        return fetchedSubCategory;    }

    @Override
    public SubCategoryInterface getTopByCodeOrderByIdDesc(long code) {
        final String methodName = "getTopByCodeOrderByIdDesc() : ";
        logger.info(methodName + "called");
        SubCategoryInterface fetchedSubCategory = null;
        if(code != 0l){
            fetchedSubCategory = subCategoryRepository.findTopByCodeOrderByIdDesc(code);
        }
        return fetchedSubCategory;
    }

    public SubCategoryInterface getByTariffIdAndCode(long tariffId, long code){
        String methodName = "getByTariffIdAndCode() : ";
        logger.info(methodName + "called for tariffId " + tariffId + " code " + code);
        SubCategoryInterface subCategory = subCategoryRepository.findByTariffIdAndCode(tariffId,code);
        return subCategory;
    }
}
