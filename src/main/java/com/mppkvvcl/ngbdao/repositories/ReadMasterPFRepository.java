package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ReadMasterPFInterface;
import com.mppkvvcl.ngbentity.beans.ReadMasterPF;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by PREETESH on 6/30/2017.
 */
@Repository
public interface ReadMasterPFRepository extends JpaRepository<ReadMasterPF, Long> {
    public ReadMasterPFInterface findByReadMasterId(Long readMasterId);
    public ReadMasterPFInterface save(ReadMasterPFInterface readMasterPFInterface);
}
