package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface InstrumentDetailDAOInterface<T> extends DAOInterface<T>{

    public List<T> getByDishonoured(boolean dishonoured);
    public T getByPayModeAndBankNameAndInstrumentNo(String payMode, String bankName, String instrumentNo);
    public void deleteById(long id);
    public List<T> getByInstrumentNo(final String instrumentNo);
}