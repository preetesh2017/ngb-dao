package com.mppkvvcl.ngbdao.interfaces;

import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
public interface BillDAOInterface<T> extends DAOInterface<T>{

    public T getTopByConsumerNoAndDeleted(String consumerNo, boolean deleted);

    public List<T> getByConsumerNoAndBillMonthAndDeleted(String consumerNo, String billMonth, boolean deleted);

    List<String> getDistinctBillMonthByConsumerNoOrderByIdDesc(String consumerNo, boolean deleted);

    public T getTopByConsumerNoAndDeletedOrderByIdDesc(String consumerNo, boolean deleted) ;

    public List<T> getAllByConsumerNo(String consumerNo, boolean deleted);

    public List<T> getByConsumerNoOrderByBillMonthDescendingWithPageable(String consumerNo, Pageable pageable);

    public List<T> getByConsumerNoOrderByBillMonthAscendingWithPageable(String consumerNo, Pageable pageable);

    public long getCountByConsumerNo(String consumerNo);

    public List<T> getByConsumerNo(String consumerNo, Pageable pageable);

    public List<String> getDistinctBillMonthByLocationCode(String locationCode);

    public List<String> getDistinctGroupNoByLocationCodeAndBillMonth(String locationCode, String billMonth);

    public List<T> getByBillMonthAndGroupNoAndDeleted(String billMonth, String groupNo, boolean deleted);

    public List<T> getByConsumerNoAndBillMonthOrderByIdAsc(String consumerNo, String billMonth);
}
