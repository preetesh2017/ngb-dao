package com.mppkvvcl.ngbdao.interfaces;
import java.util.Date;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface EmployeeConnectionMappingDAOInterface<T> extends DAOInterface<T>{

    public List<T> getByEmployeeNo(String employeeNo);
    public List<T> getByConsumerNo(String consumerNo);
    public List<T> getByStatus(String status);
    public List<T> getByEmployeeNoAndStatus(String employeeNo, String Status);
    public List<T> getByConsumerNoAndStatus(String consumerNo, String Status);
    public List<T> getByEmployeeNoAndConsumerNoAndStatus(String employeeNo, String consumerNo, String Status);
    public List<T> getByStartDateBetween(Date startDate, Date endDate);
    public List<T> getByEndDateBetween(Date startDate, Date endDate);
    public List<T> getByStartBillMonthBetween(String startBillMonth,String endBillMonth);
    public List<T> getByEndBillMonthBetween(String startBillMonth,String endBillMonth);

}
