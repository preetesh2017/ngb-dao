package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.GroupInterface;
import com.mppkvvcl.ngbentity.beans.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PREETESH on 9/1/2017.
 */
@Repository
public interface GroupRepository extends JpaRepository<Group,Long>{

    public List<GroupInterface> findByLocationCode(String locationCode);

    public GroupInterface save(GroupInterface group);

    public GroupInterface findByGroupNo(String groupNo);
}
