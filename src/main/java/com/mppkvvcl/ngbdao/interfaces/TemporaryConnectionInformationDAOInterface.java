package com.mppkvvcl.ngbdao.interfaces;
import java.util.Date;
import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
public interface TemporaryConnectionInformationDAOInterface<T> extends DAOInterface<T> {
    public List<T> getByConsumerNo(String consumerNo);
    public List<T> getBetweenTcStartDateAndTcEndDate(Date givenDate);
}
