package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbinterface.interfaces.AdjustmentProfileInterface;
import com.mppkvvcl.ngbdao.interfaces.AdjustmentProfileDAOInterface;
import com.mppkvvcl.ngbdao.repositories.AdjustmentProfileRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
@Service
public class AdjustmentProfileDAO implements AdjustmentProfileDAOInterface<AdjustmentProfileInterface> {

    /**
     * Requesting spring to get singleton AdjustmentProfileRepository object.
     */
    @Autowired
    private AdjustmentProfileRepository adjustmentProfileRepository;

    /**
     * Getting whole logger object from global resources for current class.
     */
    private Logger logger = GlobalResources.getLogger(AdjustmentProfileDAO.class);

    @Override
    public List<AdjustmentProfileInterface> getByAdjustmentId(long adjustmentId) {
        final String methodName = "findByAdjustmentId() : ";
        logger.info(methodName + "is called");
        List<AdjustmentProfileInterface> adjustmentProfiles = null;
        adjustmentProfiles = adjustmentProfileRepository.findByAdjustmentIdOrderByIdAsc(adjustmentId);
        return adjustmentProfiles;
    }

    @Override
    public AdjustmentProfileInterface add(AdjustmentProfileInterface adjustmentProfile) {
        final String methodName = "add() : ";
        logger.info(methodName + "is called");
        AdjustmentProfileInterface insertedAdjustmentProfile = null;
        if(adjustmentProfile != null){
            insertedAdjustmentProfile = adjustmentProfileRepository.save(adjustmentProfile);
        }
        return insertedAdjustmentProfile;
    }

    @Override
    public AdjustmentProfileInterface update(AdjustmentProfileInterface adjustmentProfile) {
        final String methodName = "update() : ";
        logger.info(methodName + "is called");
        AdjustmentProfileInterface updatedAdjustmentProfile = null;
        if(adjustmentProfile != null){
            updatedAdjustmentProfile = adjustmentProfileRepository.save(adjustmentProfile);
        }
        return updatedAdjustmentProfile;
    }

    @Override
    public AdjustmentProfileInterface get(AdjustmentProfileInterface adjustmentProfile) {
        final String methodName = "get() : ";
        logger.info(methodName + "is called");
        AdjustmentProfileInterface existingAdjustmentProfile = null;
        if(adjustmentProfile != null){
            existingAdjustmentProfile = adjustmentProfileRepository.findOne(adjustmentProfile.getId());
        }
        return existingAdjustmentProfile;
    }

    @Override
    public AdjustmentProfileInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "is called");
        AdjustmentProfileInterface adjustmentProfile = null;
        adjustmentProfile = adjustmentProfileRepository.findOne(id);
        return adjustmentProfile;
    }
}
