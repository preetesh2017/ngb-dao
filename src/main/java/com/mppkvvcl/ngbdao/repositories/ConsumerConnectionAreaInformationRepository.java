package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionAreaInformationInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerConnectionAreaInformation;
import org.hibernate.persister.collection.CollectionPropertyNames;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Modified by Vikas Patel on 23-06-2017.
 */
@Repository
public interface ConsumerConnectionAreaInformationRepository extends JpaRepository<ConsumerConnectionAreaInformation,Long>{
    /**
     * To find ConsumerConnectionAreaInformation by providing Consumer Number
     * param consumerNumber
     * return ConsumerConnectionAreaInformation
     */
    public ConsumerConnectionAreaInformationInterface findByConsumerNo(String consumerNumber);

    /**
     * To find ConsumerConnectionAreaInformation by providing DTR Name
     * param dtrName
     * return List of ConsumerConnectionAreaInformation
     */
    public List<ConsumerConnectionAreaInformationInterface> findByDtrName(String dtrName);

    /**
     * To find ConsumerConnectionAreaInformation by providing Feeder Name
     * param feederName
     * return List of ConsumerConnectionAreaInformation
     */
    public List<ConsumerConnectionAreaInformationInterface> findByFeederName(String feederName);

    /**
     * To find ConsumerConnectionAreaInformation by providing Feeder Name and Dtr Name
     * param feederName
     * param dtrName
     * return List of ConsumerConnectionAreaInformation
     */
    public List<ConsumerConnectionAreaInformationInterface> findByFeederNameAndDtrName(String feederName,String dtrName);

    /**
     * To find ConsumerConnectionAreaInformation by providing Group No
     * param groupNo
     * return List of ConsumerConnectionAreaInformation
     */
    public List<ConsumerConnectionAreaInformationInterface> findByGroupNo(String groupNo);

    /**
     * To find ConsumerConnectionAreaInformation by providing Group No and Reading Diary No
     * param groupNo
     * param readingDiaryNo
     * return List of ConsumerConnectionAreaInformation
     */
    public List<ConsumerConnectionAreaInformationInterface> findByGroupNoAndReadingDiaryNo(String groupNo, String readingDiaryNo);

    /**
     * To find ConsumerConnectionAreaInformation by providing Survey Date
     * param surveyDate
     * return List of ConsumerConnectionAreaInformation
     */
    public List<ConsumerConnectionAreaInformationInterface> findBySurveyDate(Date surveyDate);

    /**
     * To find ConsumerConnectionAreaInformation between two Survey Dates
     * param startSurveyDate
     * param endSurveyDate
     * return List of ConsumerConnectionAreaInformation
     */
    @Query("from ConsumerConnectionAreaInformation c where c.surveyDate between :date1 and :date2")
    public List<ConsumerConnectionAreaInformationInterface> findBySurveyDateBetween(@Param("date1") Date startSurveyDate, @Param("date2") Date endSurveyDate);

    /**
     * To find ConsumerConnectionAreaInformation by providing Feeder Name, DTR Name and Pole No
     * param feederName
     * param dtrName
     * param poleNo
     * return List of ConsumerConnectionAreaInformation
     */
    public List<ConsumerConnectionAreaInformationInterface> findByFeederNameAndDtrNameAndPoleNo( String feederName, String dtrName,String poleNo);

    /**
     * To get ConsumerConnectionAreaInformation by providing Feeder Name and Area Status
     * param feederName
     * param areaStatus
     * return List of ConsumerConnectionAreaInformation
     */
    public List<ConsumerConnectionAreaInformationInterface> findByFeederNameAndAreaStatus(String feederName, String areaStatus);

    public ConsumerConnectionAreaInformationInterface save(ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation);

    public long countByGroupNo(String groupNo);

    public long countByGroupNoAndReadingDiaryNo(String groupNo, String readingDiaryNo);
}