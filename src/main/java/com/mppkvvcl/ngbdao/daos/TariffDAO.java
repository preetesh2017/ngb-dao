package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.TariffDAOInterface;
import com.mppkvvcl.ngbdao.repositories.TariffRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.TariffInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
@Service
public class TariffDAO implements TariffDAOInterface<TariffInterface> {

    /**
     * Requesting spring to get singleton  TariffRepository object.
     */
    @Autowired
    TariffRepository tariffRepository;

    /**
     * Getting whole logger object from global resources for current class.
     */
    Logger logger = GlobalResources.getLogger(TariffDAO.class);

    @Override
    public List<TariffInterface> getByTariffCategoryAndConnectionTypeAndEffectiveDate(String tariffCategory, String connectionType, String date) {
        final String methodName = "getByTariffCategoryAndConnectionTypeAndEffectiveDate() : ";
        logger.info(methodName + " is called");
        List<TariffInterface> tariffs = null;
        if(tariffCategory != null && connectionType != null && date != null){
            tariffs = tariffRepository.findByTariffCategoryAndConnectionTypeAndEffectiveDate(tariffCategory, connectionType, date);
        }
        return tariffs;
    }

    @Override
    public List<TariffInterface> getByTariffCategoryAndEffectiveDate(String tariffCategory, String date) {
        final String methodName = "getByTariffCategoryAndEffectiveDate() : ";
        logger.info(methodName + "is called");
        List<TariffInterface> tariffs = null;
        if(tariffCategory != null && date != null){
            tariffs = tariffRepository.findByTariffCategoryAndEffectiveDate(tariffCategory, date);
        }
        return tariffs;
    }

    @Override
    public List<TariffInterface> getByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDate(String tariffCategory, String meteringStatus, String connectionType, String date) {
        final String methodName = "getByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDate() : ";
        logger.info(methodName + "is called");
        List<TariffInterface> tariffs = null;
        if(tariffCategory != null && meteringStatus != null && connectionType != null && date != null){
            tariffs = tariffRepository.findByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDate(tariffCategory, meteringStatus, connectionType, date);
        }
        return tariffs;
    }

    @Override
    public List<TariffInterface> getByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffTypeAndEffectiveDate(String tariffCategory, String meteringStatus, String connectionType, String tariffType, String date) {
        final String methodName = "getByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffTypeAndEffectiveDate() : ";
        logger.info(methodName + "is called");
        List<TariffInterface> tariffs = null;
        if(tariffCategory != null && meteringStatus != null && connectionType != null && tariffType != null && date != null){
            tariffs = tariffRepository.findByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffTypeAndEffectiveDate(tariffCategory, meteringStatus, connectionType, tariffType, date);
        }
        return tariffs;
    }

    @Override
    public List<TariffInterface> getByTariffCategoryAndMeteringStatusAndEffectiveDate(String tariffCategory, String meteringStatus, String date) {
        final String methodName = "getByTariffCategoryAndMeteringStatusAndEffectiveDate() : ";
        logger.info(methodName + "is called");
        List<TariffInterface> tariffs = null;
        if(tariffCategory != null && meteringStatus != null && date != null){
            tariffs = tariffRepository.findByTariffCategoryAndMeteringStatusAndEffectiveDate(tariffCategory, meteringStatus, date);
        }
        return tariffs;
    }

    @Override
    public TariffInterface getByTariffCodeAndEffectiveDate(String tariffCode, String effectiveDate) {
        final String methodName = "getByTariffCodeAndEffectiveDate() : ";
        logger.info(methodName + "is called");
        TariffInterface tariff = null;
        if (tariffCode != null && effectiveDate != null){
            tariff = tariffRepository.findByTariffCodeAndEffectiveDate(tariffCode, effectiveDate);
        }
        return tariff;
    }

    @Override
    public TariffInterface getTopByTariffCodeOrderByIdDesc(String tariffCode) {
        final String methodName = "getTopByTariffCode() : ";
        logger.info(methodName + "is called");
        TariffInterface tariff = null;
        if (tariffCode != null){
            tariff = tariffRepository.findTopByTariffCodeOrderByIdDesc(tariffCode);
        }
        return tariff;
    }

    @Override
    public TariffInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "is called");
        TariffInterface tariff = null;
        tariff = tariffRepository.findOne(id);
        return tariff;
    }

    @Override
    public TariffInterface add(TariffInterface tariff) {
        final String methodName = "add() : ";
        logger.info(methodName + "is called");
        TariffInterface insertedTariff = null;
        if (tariff != null){
            insertedTariff = tariffRepository.save(tariff);
        }
        return insertedTariff;
    }

    @Override
    public TariffInterface get(TariffInterface tariff) {
        final String methodName = "get() : ";
        logger.info(methodName + "is called");
        TariffInterface existingTariff = null;
        if (tariff != null){
            existingTariff = tariffRepository.findOne(tariff.getId());
        }
        return existingTariff;
    }

    @Override
    public TariffInterface update(TariffInterface tariff) {
        final String methodName = "update() : ";
        logger.info(methodName + "is called");
        TariffInterface updatedTariff = null;
        if (tariff != null){
            updatedTariff = tariffRepository.save(tariff);
        }
        return updatedTariff;
    }

    @Override
    public List<? extends TariffInterface> getAll() {
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        List<? extends TariffInterface> tariffs = tariffRepository.findAll();
        return tariffs;
    }

    @Override
    public List<TariffInterface> getByTariffCodeAndEffectiveStartDateAndEffectiveEndDate(String tariffCode, String effectiveStartDate, String effectiveEndDate) {
        String methodName = "getByTariffCodeAndEffectiveStartDateAndEffectiveEndDate() : ";
        logger.info(methodName + "called");
        List<TariffInterface> tariffInterfaces = null;
        if(tariffCode != null && effectiveStartDate != null && effectiveEndDate != null){
            tariffInterfaces = tariffRepository.findByTariffCodeAndEffectiveStartDateAndEffectiveEndDate(tariffCode,effectiveStartDate,effectiveEndDate);
        }
        return tariffInterfaces;
    }
}
