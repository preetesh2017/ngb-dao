package com.mppkvvcl.ngbdao.interfaces;


import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
public interface ConsumerMeterMappingDAOInterface<T> extends DAOInterface<T> {

    public List<T> getByConsumerNo(String consumerNo);
    public List<T> getByMeterIdentifier(String meterIdentifier);
    public List<T> getByMeterIdentifierAndMappingStatus(String meterIdentifier, String mappingStatus);
    public T getByConsumerNoAndMeterIdentifier(String consumerNo, String meterIdentifier);
    public T getByConsumerNoAndMeterIdentifierAndMappingStatus(String consumerNo, String meterIdentifier, String mappingStatus);
    public List<T> getByMeterSerialNo(String meterSerialNo);
    public List<T> getByMeterMake(String meterMake);
    public List<T> getByConsumerNoAndMappingStatus(String consumerNo,String mappingStatus);
    public List<T> getByInstallationDateBetween(Date startDate, Date endDate);
    public List<T> getByRemovalDateBetween(Date startDate,Date endDate);
    public List<T> getByInstallationBillMonth(String installationBillMonthStart, String installationBillMonthEnd);
    public List<T> getByConsumerNoAndInstallationBillMonth( String consumerNo, String installationBillMonthStart, String installationBillMonthEnd);
    public List<T> getByRemovalBillMonth(String removalBillMonthStart, String removalBillMonthEnd);
    public List<T> getByConsumerNoAndRemovalBillMonth(String consumerNo, String removalBillMonthStart, String removalBillMonthEnd);

    //count methods
    public long getCountByConsumerNo(String consumerNo);

    public long getCountByMeterIdentifier(String meterIdentifier);

    //pageable methods
    public List<T> getByConsumerNo(String consumerNo, Pageable pageable);

    public List<T> getByConsumerNoOrderByInstallationBillMonthDescending(String consumerNo, Pageable pageable);

    public List<T> getByConsumerNoOrderByInstallationBillMonthAscending(String consumerNo, Pageable pageable);

    public List<T> getByMeterIdentifier(String meterIdentifier,Pageable pageable);

    public List<T> getByMeterIdentifierOrderByInstallationBillMonthDescending(String meterIdentifier,Pageable pageable);

    public List<T> getByMeterIdentifierOrderByInstallationBillMonthAscending(String meterIdentifier,Pageable pageable);
}
