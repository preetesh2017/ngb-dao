package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.TemporaryConnectionInformationInterface;
import com.mppkvvcl.ngbentity.beans.TemporaryConnectionInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 6/14/2017.
 */
@Repository
public interface TemporaryConnectionInformationRepository extends JpaRepository<TemporaryConnectionInformation,Long> {

    /**
     * find data of temporary consumer
     * param consumerNo
     * return
     */
    public List<TemporaryConnectionInformationInterface> findByConsumerNo(String consumerNo);

    /**
     * to find list of temporary consumers on given date
     * param givenDate
     * return
     */
    @Query("from TemporaryConnectionInformation t where :givenDate between t.tcStartDate and t.tcEndDate")
    public List<TemporaryConnectionInformationInterface> findBetweenTcStartDateAndTcEndDate(@Param ("givenDate") Date givenDate);

    public TemporaryConnectionInformationInterface save(TemporaryConnectionInformationInterface temporaryConnectionInformationInterface);
}
