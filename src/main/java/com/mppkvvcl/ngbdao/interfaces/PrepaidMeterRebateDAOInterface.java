package com.mppkvvcl.ngbdao.interfaces;

import java.util.Date;

public interface PrepaidMeterRebateDAOInterface<T> extends DAOInterface<T> {
    public T getByDate(Date date);
}
