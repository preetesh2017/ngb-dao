package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.TemporaryConnectionInformationDAOInterface;
import com.mppkvvcl.ngbdao.repositories.TemporaryConnectionInformationRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.TemporaryConnectionInformationInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
@Service
public class TemporaryConnectionInformationDAO implements TemporaryConnectionInformationDAOInterface<TemporaryConnectionInformationInterface> {
    @Autowired
    TemporaryConnectionInformationRepository temporaryConnectionInformationRepository;
    /**
     *
     */
    Logger logger = GlobalResources.getLogger(TemporaryConnectionInformationDAO.class);

    @Override
    public List<TemporaryConnectionInformationInterface> getBetweenTcStartDateAndTcEndDate(Date givenDate) {
        final String methodName = "getBetweenTcStartDateAndTcEndDate() : ";
        logger.info(methodName + "called");
        List<TemporaryConnectionInformationInterface> temporaryConnectionInformations = null;
        if(givenDate != null){
            temporaryConnectionInformations = temporaryConnectionInformationRepository.findBetweenTcStartDateAndTcEndDate(givenDate);
        }
        return temporaryConnectionInformations;
    }

    @Override
    public List<TemporaryConnectionInformationInterface> getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        List<TemporaryConnectionInformationInterface> temporaryConnectionInformations = null;
        if(consumerNo != null){
            temporaryConnectionInformations = temporaryConnectionInformationRepository.findByConsumerNo(consumerNo);
        }
        return temporaryConnectionInformations;
    }

    @Override
    public TemporaryConnectionInformationInterface add(TemporaryConnectionInformationInterface temporaryConnectionInformation) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        TemporaryConnectionInformationInterface insertedTemporaryConnectionInformation = null;
        if(temporaryConnectionInformation != null){
            insertedTemporaryConnectionInformation = temporaryConnectionInformationRepository.save(temporaryConnectionInformation);
        }
        return insertedTemporaryConnectionInformation;
    }

    @Override
    public TemporaryConnectionInformationInterface get(TemporaryConnectionInformationInterface temporaryConnectionInformation) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        TemporaryConnectionInformationInterface existingTemporaryConnectionInformation = null;
        if(temporaryConnectionInformation != null){
            existingTemporaryConnectionInformation = temporaryConnectionInformationRepository.findOne(temporaryConnectionInformation.getId());
        }
        return existingTemporaryConnectionInformation;
    }

    @Override
    public TemporaryConnectionInformationInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        TemporaryConnectionInformationInterface temporaryConnectionInformation = null;
        temporaryConnectionInformation = temporaryConnectionInformationRepository.findOne(id);
        return temporaryConnectionInformation;
    }

    @Override
    public TemporaryConnectionInformationInterface update(TemporaryConnectionInformationInterface temporaryConnectionInformation) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        TemporaryConnectionInformationInterface updatedTemporaryConnectionInformation = null;
        if(temporaryConnectionInformation != null){
            updatedTemporaryConnectionInformation = temporaryConnectionInformationRepository.save(temporaryConnectionInformation);
        }
        return updatedTemporaryConnectionInformation;
    }
}
