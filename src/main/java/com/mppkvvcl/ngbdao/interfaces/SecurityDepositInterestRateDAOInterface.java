package com.mppkvvcl.ngbdao.interfaces;

import java.util.Date;
import java.util.List;

public interface SecurityDepositInterestRateDAOInterface<T> extends DAOInterface<T> {

    public List<T> getByEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(Date startDate, Date endDate);
}
