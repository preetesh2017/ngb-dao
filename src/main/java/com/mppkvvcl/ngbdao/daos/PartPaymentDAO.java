package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.PartPaymentDAOInterface;
import com.mppkvvcl.ngbdao.repositories.PartPaymentRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PartPaymentInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
@Service
public class PartPaymentDAO implements PartPaymentDAOInterface<PartPaymentInterface> {

    /**
     * Requesting spring to get singleton  PartPaymentRepository object.
     */
    @Autowired
    PartPaymentRepository partPaymentRepository;

    /**
     * Getting whole logger object from global resources for current class.
     */
    Logger logger = GlobalResources.getLogger(PartPaymentDAO.class);

    @Override
    public List<PartPaymentInterface> getByConsumerNoAndAppliedOrderByIdDesc(String consumerNo, boolean applied) {
        final String methodName = "getByConsumerNoAndAppliedOrderByIdDesc() : ";
        logger.info(methodName + "called");
        List<PartPaymentInterface> partPayments = null;
        if(consumerNo != null){
            partPayments = partPaymentRepository.findByConsumerNoAndAppliedOrderByIdDesc(consumerNo, applied);
        }
        return partPayments;
    }

    @Override
    public PartPaymentInterface getByConsumerNoAndAppliedAndUpdatedOn(String consumerNo, boolean appliedTrue, Date updatedOn) {
        final String methodName = "getByConsumerNoAndAppliedAndUpdatedOn() : ";
        logger.info(methodName + "called for consumer " + consumerNo + " date " + updatedOn);
        PartPaymentInterface partPayment = null;
        if(consumerNo != null && updatedOn != null){
            partPayment = partPaymentRepository.findByConsumerNoAndAppliedAndUpdatedOn(consumerNo,appliedTrue,updatedOn);
        }
        return partPayment;
    }

    @Override
    public List<PartPaymentInterface> getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called ");
        List<PartPaymentInterface> partPayments = null;
        if(consumerNo != null ){
            partPayments = partPaymentRepository.findByConsumerNo(consumerNo);
        }
        return partPayments;
    }


    @Override
    public PartPaymentInterface add(PartPaymentInterface partPayment) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        PartPaymentInterface insertedPartPayment = null;
        if(partPayment != null){
            insertedPartPayment = partPaymentRepository.save(partPayment);
        }
        return insertedPartPayment;
    }

    @Override
    public PartPaymentInterface get(PartPaymentInterface partPayment) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        PartPaymentInterface existingPartPayment = null;
        if(partPayment != null){
            existingPartPayment = partPaymentRepository.findOne(partPayment.getId());
        }
        return existingPartPayment;
    }

    @Override
    public PartPaymentInterface update(PartPaymentInterface partPayment) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        PartPaymentInterface updatedPartPayment = null;
        if(partPayment != null){
            updatedPartPayment = partPaymentRepository.save(partPayment);
        }
        return updatedPartPayment;
    }

    @Override
    public PartPaymentInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        PartPaymentInterface partPayment = null;
        partPayment = partPaymentRepository.findOne(id);
        return partPayment;
    }

    @Override
    public boolean deleteById(long id) {
        final String methodName = "deleteById() : ";
        logger.info(methodName + "called");
        if(id > 0){
            try{
                partPaymentRepository.delete(id);
                return true;
            }catch (Exception e){
                logger.error(e.toString());
                return false;
            }
        }
        return false;
    }
}
