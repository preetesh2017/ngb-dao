package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
public interface AdjustmentTypeDAOInterface<T> extends DAOInterface<T> {

    public List<T> getAllByView(boolean view);

    public List<? extends T> getAll();

    public T getByCode(int code);
}
