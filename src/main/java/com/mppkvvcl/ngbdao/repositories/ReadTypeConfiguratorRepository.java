package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ReadTypeConfiguratorInterface;
import com.mppkvvcl.ngbentity.beans.ReadTypeConfigurator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by PREETESH on 7/1/2017.
 */
@Repository
public interface ReadTypeConfiguratorRepository extends JpaRepository<ReadTypeConfigurator, Long>{
    public ReadTypeConfiguratorInterface findByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode);

    public ReadTypeConfiguratorInterface save(ReadTypeConfiguratorInterface readTypeConfiguratorInterface);
}
