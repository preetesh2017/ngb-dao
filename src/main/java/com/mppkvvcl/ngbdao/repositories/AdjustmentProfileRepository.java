package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.AdjustmentProfileInterface;
import com.mppkvvcl.ngbentity.beans.AdjustmentProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ANKIT on 19-08-2017.
 */
@Repository
public interface AdjustmentProfileRepository extends JpaRepository<AdjustmentProfile, Long> {
    public List<AdjustmentProfileInterface> findByAdjustmentIdOrderByIdAsc(long adjustmentId);
    public AdjustmentProfileInterface save(AdjustmentProfileInterface adjustmentProfileInterface);
}
