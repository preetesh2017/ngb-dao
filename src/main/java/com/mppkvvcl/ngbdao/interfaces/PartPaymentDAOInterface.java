package com.mppkvvcl.ngbdao.interfaces;
import java.util.Date;
import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
public interface PartPaymentDAOInterface<T> extends DAOInterface<T> {
    public List<T> getByConsumerNoAndAppliedOrderByIdDesc(String consumerNo, boolean applied);
    public T getByConsumerNoAndAppliedAndUpdatedOn(String consumerNo, boolean appliedTrue, Date updatedOn);
    public List<T> getByConsumerNo(String consumerNo);
    public boolean deleteById(long id);
}
