package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbinterface.interfaces.AdjustmentRangeInterface;
import com.mppkvvcl.ngbdao.interfaces.AdjustmentRangeDAOInterface;
import com.mppkvvcl.ngbdao.repositories.AdjustmentRangeRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;


/**
 * Created by ANSHIKA on 15-09-2017.
 */
@Service
public class AdjustmentRangeDAO implements AdjustmentRangeDAOInterface<AdjustmentRangeInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private Logger logger = GlobalResources.getLogger(AdjustmentRangeDAO.class);

    /**
     * Requesting spring to get singleton AdjustmentRansgeRepository object.
     */
    @Autowired
    private AdjustmentRangeRepository adjustmentRangeRepository;

    @Override
    public AdjustmentRangeInterface getByCodeAndAmount(int code, BigDecimal amount) {
        final String methodName = "getByCodeAndAmount() : ";
        logger.info(methodName + "is called");
        AdjustmentRangeInterface adjustmentRange = null;
        if(code > 0 && amount != null){
            adjustmentRange = adjustmentRangeRepository.findByCodeAndAmount(code,amount);
        }
        return adjustmentRange;
    }

    @Override
    public AdjustmentRangeInterface get(AdjustmentRangeInterface adjustmentRange) {
        final String methodName = "get() : ";
        logger.info(methodName + "is called");
        AdjustmentRangeInterface existingAdjustmentRange = null;
        if(adjustmentRange != null){
            existingAdjustmentRange = adjustmentRangeRepository.findOne(adjustmentRange.getId());
        }
        return existingAdjustmentRange;
    }

    @Override
    public AdjustmentRangeInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "is called");
        AdjustmentRangeInterface adjustmentRange = null;
        adjustmentRange = adjustmentRangeRepository.findOne(id);
        return adjustmentRange;
    }

    @Override
    public AdjustmentRangeInterface add(AdjustmentRangeInterface adjustmentRange) {
        final String methodName = "add() : ";
        logger.info(methodName + "is called");
        AdjustmentRangeInterface insertedAdjustmentRange = null;
        if(adjustmentRange != null){
            insertedAdjustmentRange = adjustmentRangeRepository.save(adjustmentRange);
        }
        return insertedAdjustmentRange;
    }

    @Override
    public AdjustmentRangeInterface update(AdjustmentRangeInterface adjustmentRange) {
        final String methodName = "update() : ";
        logger.info(methodName + "is called");
        AdjustmentRangeInterface updatedAdjustmentRange = null;
        if(adjustmentRange != null){
            updatedAdjustmentRange = adjustmentRangeRepository.save(adjustmentRange);
        }
        return updatedAdjustmentRange;
    }
}
