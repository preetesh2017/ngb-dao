package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.MeterCTRMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.MeterCTRMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterCTRMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
@Service
public class MeterCTRMappingDAO implements MeterCTRMappingDAOInterface<MeterCTRMappingInterface> {

    Logger logger = GlobalResources.getLogger(MeterCTRMappingDAO.class);

    @Autowired
    MeterCTRMappingRepository meterCTRMappingRepository ;

    @Override
    public MeterCTRMappingInterface add(MeterCTRMappingInterface meterCTRMapping) {
        final String methodName = "add() : ";
        logger.info(methodName+"clicked");
        MeterCTRMappingInterface insertedMeterCTRMapping = null;
        if (meterCTRMapping != null){
            insertedMeterCTRMapping = meterCTRMappingRepository.save(meterCTRMapping);
        }
        return insertedMeterCTRMapping;
    }

    @Override
    public MeterCTRMappingInterface update(MeterCTRMappingInterface meterCTRMapping) {
        final String methodName = "update() : ";
        logger.info(methodName+"clicked");
        MeterCTRMappingInterface updatedMeterCTRMapping = null;
        if (meterCTRMapping != null){
            updatedMeterCTRMapping = meterCTRMappingRepository.save(meterCTRMapping);
        }
        return updatedMeterCTRMapping;
    }

    @Override
    public MeterCTRMappingInterface get(MeterCTRMappingInterface meterCTRMapping) {
        final String methodName = "get() : ";
        logger.info(methodName+"clicked");
        MeterCTRMappingInterface getMeterCTRMapping = null;
        if (meterCTRMapping != null){
            getMeterCTRMapping = meterCTRMappingRepository.findOne(meterCTRMapping.getId());
        }
        return getMeterCTRMapping;
    }

    @Override
    public MeterCTRMappingInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName+"clicked");
        MeterCTRMappingInterface getMeterCTRMapping = null;
        if (id > 0){
            getMeterCTRMapping = meterCTRMappingRepository.findOne(id);
        }
        return getMeterCTRMapping;
    }

    @Override
    public List<MeterCTRMappingInterface> getByMeterIdentifier(String meterIdentifier) {
        final String methodName = "getByMeterIdentifier() : ";
        logger.info(methodName+"clicked");
        List<MeterCTRMappingInterface> meterCTRMapping = null;
        if (meterIdentifier != null){
            meterCTRMapping = meterCTRMappingRepository.findByMeterIdentifier(meterIdentifier);
        }
        return meterCTRMapping;
    }

    @Override
    public List<MeterCTRMappingInterface> getByMeterIdentifierAndMappingStatus(String meterIdentifier, String mappingStatus) {
        final String methodName = "getByMeterIdentifierAndMappingStatus() : ";
        logger.info(methodName+"clicked");
        List<MeterCTRMappingInterface> meterCTRMapping = null;
        if (meterIdentifier != null && mappingStatus != null){
            meterCTRMapping = meterCTRMappingRepository.findByMeterIdentifierAndMappingStatus(meterIdentifier , mappingStatus);
        }
        return meterCTRMapping;
    }

    @Override
    public List<MeterCTRMappingInterface> getByCtrIdentifierAndMappingStatus(String ctrIdentifier, String mappingStatus) {
        final String methodName = "getByCtrIdentifierAndMappingStatus() : ";
        logger.info(methodName+"clicked");
        List<MeterCTRMappingInterface> meterCTRMapping = null;
        if (ctrIdentifier != null && mappingStatus != null){
            meterCTRMapping = meterCTRMappingRepository.findByCtrIdentifierAndMappingStatus(ctrIdentifier , mappingStatus);
        }
        return meterCTRMapping;
    }
}
