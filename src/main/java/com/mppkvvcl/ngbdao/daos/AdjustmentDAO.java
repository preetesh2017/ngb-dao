package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.AdjustmentDAOInterface;
import com.mppkvvcl.ngbdao.repositories.AdjustmentRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.AdjustmentInterface;
import com.mppkvvcl.ngbinterface.interfaces.BillInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
@Service
public class AdjustmentDAO implements AdjustmentDAOInterface<AdjustmentInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private Logger logger = GlobalResources.getLogger(AdjustmentDAO.class);

    /**
     * Requesting spring to get singleton AdjustmentRepository object.
     */
    @Autowired
    private AdjustmentRepository adjustmentRepository;

    @Override
    public AdjustmentInterface getByConsumerNoAndPostingBillMonth(String consumerNo, String postingBillMonth) {
        final String methodName = "getByConsumerNoAndPostingBillMonth() : ";
        logger.info(methodName + "is called");
        AdjustmentInterface adjustment = null;
        if(consumerNo != null && postingBillMonth != null){
            adjustment = adjustmentRepository.findByConsumerNoAndPostingBillMonth(consumerNo, postingBillMonth);
        }
        return adjustment;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndPostedAndDeleted(String consumerNo, boolean posted, boolean deleted) {
        final String methodName = "getByConsumerNoAndPostedAndDeleted() : ";
        logger.info(methodName + "is called");
        List<AdjustmentInterface> adjustments = null;
        if(consumerNo != null){
            adjustments = adjustmentRepository.findByConsumerNoAndPostedAndDeleted(consumerNo, posted, deleted);
        }
        return adjustments;
    }

    @Override
    public AdjustmentInterface get(AdjustmentInterface adjustment) {
        final String methodName = "get() : ";
        logger.info(methodName + "is called");
        AdjustmentInterface existingAdjustment = null;
        if(adjustment != null)
        {
            existingAdjustment = adjustmentRepository.findOne(adjustment.getId());
        }
        return existingAdjustment;
    }

    @Override
    public AdjustmentInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "is called");
        AdjustmentInterface adjustment = null;
        adjustment = adjustmentRepository.findOne(id);
        return adjustment;
    }

    @Override
    public AdjustmentInterface update(AdjustmentInterface adjustment) {
        final String methodName = "update() : ";
        logger.info(methodName + "is called");
        AdjustmentInterface updatedAdjustment = null;
        if(adjustment != null){
            updatedAdjustment = adjustmentRepository.save(adjustment);
        }
        return updatedAdjustment;
    }

    @Override
    public AdjustmentInterface add(AdjustmentInterface adjustment) {
        final String methodName = "add() : ";
        logger.info(methodName + "is called");
        AdjustmentInterface insertedAdjustment = null;
        if(adjustment != null){
            insertedAdjustment = adjustmentRepository.save(adjustment);
        }
        return insertedAdjustment;
    }

    @Override
    public List<AdjustmentInterface> add(List<AdjustmentInterface> adjustments) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        List<AdjustmentInterface> insertedAdjustments = null;
        if(adjustments != null){
            insertedAdjustments = new ArrayList<>();
            for(AdjustmentInterface adjustmentInterface : adjustments){
                insertedAdjustments.add(adjustmentRepository.save(adjustmentInterface));
            }
        }
        return insertedAdjustments;
    }

    @Override
    public List<AdjustmentInterface> getByLocationCodeAndPostedAndDeleted(String locationCode, boolean posted, boolean deleted) {
        final String methodName = "getByLocationCodeAndPostedAndDeleted() : ";
        logger.info(methodName + "called");
        List<AdjustmentInterface> adjustments = null;
        if(locationCode != null){
            adjustments = adjustmentRepository.findByLocationCodeAndPostedAndDeleted(locationCode,posted,deleted);
        }
        return adjustments;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndCodeAndPostedAndDeleted(String consumerNo, int code, boolean posted, boolean deleted) {
        final String methodName = "getByCodeAndPostedAndDeleted() : ";
        logger.info(methodName + "called");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        adjustmentInterfaces = adjustmentRepository.findByConsumerNoAndCodeAndPostedAndDeleted(consumerNo,code,posted,deleted);
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndPostedAndDeletedAndApprovalStatus(String consumerNo, boolean posted, boolean deleted, String approvalStatus) {
        final String methodName = "getByConsumerNoAndPostedAndDeletedAndApprovalStatus() : ";
        logger.info(methodName + "called");
        List<AdjustmentInterface> adjustments = null;
        if (consumerNo != null) {
            adjustments = adjustmentRepository.findByConsumerNoAndPostedAndDeletedAndApprovalStatus(consumerNo, posted, deleted, approvalStatus);
        }
        return adjustments;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndApprovalStatus(String consumerNo, String approvalStatus) {
        final String methodName = "getByConsumerNoAndApprovalStatus() : ";
        logger.info(methodName + "called");
        List<AdjustmentInterface> adjustments = null;
        if (consumerNo != null && approvalStatus != null) {
            adjustments = adjustmentRepository.findByConsumerNoAndApprovalStatus(consumerNo, approvalStatus);
        }
        return adjustments;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndPostingBillMonthAndPostedAndDeleted(String consumerNo,String postingBillMonth, boolean posted, boolean deleted){
        final String methodName = "getByConsumerNoAndPostingBillMonthAndPostedAndDeleted() : ";
        logger.info(methodName + "Called");
        List<AdjustmentInterface> adjustments = null;
        adjustments = adjustmentRepository.findByConsumerNoAndPostingBillMonthAndPostedAndDeleted(consumerNo,postingBillMonth,posted,deleted);
        return adjustments;
    }

    //count methods
    @Override
    public long getCountByConsumerNo(String consumerNo) {
        final String methodName = "getCountByConsumerNo() : ";
        logger.info(methodName + "called");
        long count = -1;
        if(consumerNo != null){
            count = adjustmentRepository.countByConsumerNo(consumerNo);
        }
        return count;
    }

    @Override
    public long getCountByConsumerNoAndPostedAndDeleted(String consumerNo, boolean posted, boolean deleted) {
        final String methodName = "getCountByConsumerNoAndPostedAndDeleted() : ";
        logger.info(methodName + "called");
        long count = -1;
        if(consumerNo != null){
            count = adjustmentRepository.countByConsumerNoAndPostedAndDeleted(consumerNo,posted,deleted);
        }
        return count;
    }

    @Override
    public long getCountByConsumerNoAndPostedAndDeletedAndApprovalStatus(String consumerNo, boolean posted, boolean deleted, String approvalStatus) {
        final String methodName = "getCountByConsumerNoAndPostedAndDeletedAndApprovalStatus() : ";
        logger.info(methodName + "called");
        long count = -1;
        if(consumerNo != null && approvalStatus != null){
            count = adjustmentRepository.countByConsumerNoAndPostedAndDeletedAndApprovalStatus(consumerNo,posted,deleted,approvalStatus);
        }
        return count;
    }

    @Override
    public long getCountByConsumerNoAndApprovalStatus(String consumerNo, String approvalStatus) {
        final String methodName = "getCountByConsumerNoAndApprovalStatus() : ";
        logger.info(methodName + "called");
        long count = -1;
        if(consumerNo != null && approvalStatus != null){
            count = adjustmentRepository.countByConsumerNoAndApprovalStatus(consumerNo,approvalStatus);
        }
        return count;
    }

    @Override
    public long getCountByLocationCodeAndPostedAndDeleted(String locationCode, boolean posted, boolean deleted) {
        final String methodName = "getCountByLocationCodeAndPostedAndDeleted() : ";
        logger.info(methodName + "called");
        long count = -1;
        if(locationCode != null){
            count = adjustmentRepository.countByLocationCodeAndPostedAndDeleted(locationCode,posted,deleted);
        }
        return count;
    }

    @Override
    public long getCountByConsumerNoAndCodeAndPostedAndDeleted(String consumerNo, int code, boolean posted, boolean deleted) {
        final String methodName = "getCountByConsumerNoAndCodeAndPostedAndDeleted() : ";
        logger.info(methodName + "called");
        long count = -1;
        if(consumerNo != null){
            count = adjustmentRepository.countByConsumerNoAndPostedAndDeleted(consumerNo,posted,deleted);
        }
        return count;
    }

    //pageable methods
    @Override
    public List<AdjustmentInterface> getByConsumerNo(String consumerNo, Pageable pageable) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByConsumerNo(consumerNo,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoOrderByPostingBillMonthDescending(String consumerNo, Pageable pageable) {
        final String methodName = "getByConsumerNoOrderByPostingBillMonthDescending() : ";
        logger.info(methodName + "called");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByConsumerNoOrderByPostingBillMonthDescending(consumerNo,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoOrderByPostingBillMonthAscending(String consumerNo, Pageable pageable) {
        final String methodName = "getByConsumerNoOrderByPostingBillMonthAscending() : ";
        logger.info(methodName + "called");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByConsumerNoOrderByPostingBillMonthAscending(consumerNo,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndPostedAndDeleted(String consumerNo, boolean posted, boolean deleted, Pageable pageable) {
        final String methodName = "getByConsumerNoAndPostedAndDeleted() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByConsumerNoAndPostedAndDeleted(consumerNo,posted,deleted,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndPostedAndDeletedOrderByPostingBillMonthDescending(String consumerNo, boolean posted, boolean deleted, Pageable pageable) {
        final String methodName = "getByConsumerNoAndPostedAndDeletedOrderByPostingBillMonthDescending() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByConsumerNoAndPostedAndDeletedOrderByPostingBillMonthDescending(consumerNo,posted,deleted,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndPostedAndDeletedOrderByPostingBillMonthAscending(String consumerNo, boolean posted, boolean deleted, Pageable pageable) {
        final String methodName = "getByConsumerNoAndPostedAndDeletedOrderByPostingBillMonthAscending() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByConsumerNoAndPostedAndDeletedOrderByPostingBillMonthAscending(consumerNo,posted,deleted,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndPostedAndDeletedAndApprovalStatus(String consumerNo, boolean posted, boolean deleted, String approvalStatus, Pageable pageable) {
        final String methodName = "getByConsumerNoAndPostedAndDeletedAndApprovalStatus() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && approvalStatus != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByConsumerNoAndPostedAndDeletedAndApprovalStatus(consumerNo,posted,deleted,approvalStatus,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndPostedAndDeletedAndApprovalStatusOrderByPostingBillMonthDescending(String consumerNo, boolean posted, boolean deleted, String approvalStatus, Pageable pageable) {
        final String methodName = "getByConsumerNoAndPostedAndDeletedAndApprovalStatusOrderByPostingBillMonthDescending() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && approvalStatus != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByConsumerNoAndPostedAndDeletedAndApprovalStatusOrderByPostingBillMonthDescending(consumerNo,posted,deleted,approvalStatus,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndPostedAndDeletedAndApprovalStatusOrderByPostingBillMonthAscending(String consumerNo, boolean posted, boolean deleted, String approvalStatus, Pageable pageable) {
        final String methodName = "getByConsumerNoAndPostedAndDeletedAndApprovalStatusOrderByPostingBillMonthAscending() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && approvalStatus != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByConsumerNoAndPostedAndDeletedAndApprovalStatusOrderByPostingBillMonthAscending(consumerNo,posted,deleted,approvalStatus,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndApprovalStatus(String consumerNo, String approvalStatus, Pageable pageable) {
        final String methodName = "getByConsumerNoAndApprovalStatus() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && approvalStatus != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByConsumerNoAndApprovalStatus(consumerNo,approvalStatus,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndApprovalStatusOrderByPostingBillMonthDescending(String consumerNo, String approvalStatus, Pageable pageable) {
        final String methodName = "getByConsumerNoAndApprovalStatusOrderByPostingBillMonthDescending() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && approvalStatus != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByConsumerNoAndApprovalStatusOrderByPostingBillMonthDescending(consumerNo,approvalStatus,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndApprovalStatusOrderByPostingBillMonthAscending(String consumerNo, String approvalStatus, Pageable pageable) {
        final String methodName = "getByConsumerNoAndApprovalStatusOrderByPostingBillMonthAscending() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && approvalStatus != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByConsumerNoAndApprovalStatusOrderByPostingBillMonthAscending(consumerNo,approvalStatus,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByLocationCodeAndPostedAndDeleted(String locationCode, boolean posted, boolean deleted, Pageable pageable) {
        final String methodName = "getByLocationCodeAndPostedAndDeleted() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(locationCode != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByLocationCodeAndPostedAndDeleted(locationCode,posted,deleted,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByLocationCodeAndPostedAndDeletedOrderByPostingBillMonthDescending(String locationCode, boolean posted, boolean deleted, Pageable pageable) {
        final String methodName = "getByLocationCodeAndPostedAndDeletedOrderByPostingBillMonthDescending() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(locationCode != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByLocationCodeAndPostedAndDeletedOrderByPostingBillMonthDescending(locationCode,posted,deleted,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByLocationCodeAndPostedAndDeletedOrderByPostingBillMonthAscending(String locationCode, boolean posted, boolean deleted, Pageable pageable) {
        final String methodName = "getByLocationCodeAndPostedAndDeletedOrderByPostingBillMonthAscending() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(locationCode != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByLocationCodeAndPostedAndDeletedOrderByPostingBillMonthAscending(locationCode,posted,deleted,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndCodeAndPostedAndDeleted(String consumerNo, int code, boolean posted, boolean deleted, Pageable pageable) {
        final String methodName = "getByConsumerNoAndCodeAndPostedAndDeleted() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByConsumerNoAndCodeAndPostedAndDeleted(consumerNo,code,posted,deleted,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndCodeAndPostedAndDeletedOrderByPostingBillMonthDescending(String consumerNo, int code, boolean posted, boolean deleted, Pageable pageable) {
        final String methodName = "getByConsumerNoAndCodeAndPostedAndDeletedOrderByPostingBillMonthDescending() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByConsumerNoAndCodeAndPostedAndDeletedOrderByPostingBillMonthDescending(consumerNo,code,posted,deleted,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndCodeAndPostedAndDeletedOrderByPostingBillMonthAscending(String consumerNo, int code, boolean posted, boolean deleted, Pageable pageable) {
        final String methodName = "getByConsumerNoAndCodeAndPostedAndDeletedOrderByPostingBillMonthAscending() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && pageable != null){
            adjustmentInterfaces = adjustmentRepository.findByConsumerNoAndCodeAndPostedAndDeletedOrderByPostingBillMonthAscending(consumerNo,code,posted,deleted,pageable);
        }
        return adjustmentInterfaces;
    }

    @Override
    public List<AdjustmentInterface> getByConsumerNoAndPostingBillMonthAndPostedAndDeleted(String consumerNo, String postingBillMonth, boolean posted, boolean deleted, Pageable pageable) {
        final String methodName = "getByConsumerNoAndPostingBillMonthAndPostedAndDeleted() : ";
        logger.info(methodName + "called with pageable");
        List<AdjustmentInterface> adjustmentInterfaces = null;
        if(consumerNo != null && postingBillMonth != null) {
            adjustmentInterfaces = adjustmentRepository.findByConsumerNoAndPostingBillMonthAndPostedAndDeleted(consumerNo,postingBillMonth,posted,deleted,pageable);
        }
        return adjustmentInterfaces;
    }
}
