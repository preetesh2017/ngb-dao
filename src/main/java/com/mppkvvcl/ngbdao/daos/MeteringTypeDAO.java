package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.MeteringTypeDAOInterface;
import com.mppkvvcl.ngbdao.repositories.MeteringTypeRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeteringTypeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
@Service
public class MeteringTypeDAO implements MeteringTypeDAOInterface<MeteringTypeInterface> {

    private Logger logger = GlobalResources.getLogger(MeteringTypeDAO.class);

    @Autowired
    private MeteringTypeRepository meteringTypeRepository;

    @Override
    public MeteringTypeInterface add(MeteringTypeInterface meteringType) {
        final String methodName = "add() :  ";
        logger.info(methodName + "clicked");
        MeteringTypeInterface insertedMeteringType = null;
        if (meteringType != null){
            insertedMeteringType = meteringTypeRepository.save(meteringType);
        }
        return insertedMeteringType;
    }

    @Override
    public MeteringTypeInterface update(MeteringTypeInterface meteringType) {
        final String methodName = "update() :  ";
        logger.info(methodName+"clicked");
        MeteringTypeInterface updatedMeteringType = null;
        if (meteringType != null){
            updatedMeteringType = meteringTypeRepository.save(meteringType);
        }
        return updatedMeteringType;
    }

    @Override
    public MeteringTypeInterface get(MeteringTypeInterface meteringType) {
        final String methodName = "get() :  ";
        logger.info(methodName+"clicked");
        MeteringTypeInterface getMeteringType = null;
        if (meteringType != null){
            getMeteringType = meteringTypeRepository.findOne(meteringType.getId());
        }
        return getMeteringType;
    }

    @Override
    public MeteringTypeInterface getById(long id) {
        final String methodName = "getById() :  ";
        logger.info(methodName+"clicked");
        MeteringTypeInterface meteringType = null;
        if (id > 0){
            meteringType = meteringTypeRepository.findOne(id);
        }
        return meteringType;
    }

    @Override
    public List<? extends MeteringTypeInterface> getAll() {
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        return meteringTypeRepository.findAll();
    }
}
