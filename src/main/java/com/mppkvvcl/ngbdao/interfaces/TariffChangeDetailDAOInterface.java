package com.mppkvvcl.ngbdao.interfaces;
/**
 * Created by vikas on 10/6/2017.
 */
public interface TariffChangeDetailDAOInterface<T> extends DAOInterface<T>{
    public T getByTariffDetailId(long tariffDetailId);
}
