package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.Bill;
import com.mppkvvcl.ngbinterface.interfaces.BillInterface;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PREETESH on 7/31/2017.
 */
@Repository
public interface BillRepository extends JpaRepository<Bill, Long>{

    public BillInterface findTopByConsumerNoAndDeletedOrderByIdDesc(String consumerNo, boolean deleted);

    @Query("select billMonth from #{#entityName} b where b.consumerNo =:consumerNo and b.deleted=:deleted order by id desc")
    public List<String> findDistinctBillMonthByConsumerNoOrderByIdDesc(@Param("consumerNo") String consumerNo,@Param("deleted") boolean deleted);

    public List<BillInterface> findByConsumerNoAndBillMonthAndDeleted(String consumerNo,String billMonth, boolean deleted);

    public List<BillInterface> findByConsumerNoAndDeletedOrderByIdDesc(String consumerNo, boolean deleted);

    public BillInterface save(BillInterface billInterface);

    /**
     * Pageable Method returns list on the basis of pageable properties coded by nitish
     */
    @Query("from #{#entityName} b where b.consumerNo= :consumerNo order by to_date(b.billMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') DESC")
    public List<BillInterface> findByConsumerNoOrderByBillMonthDescendingWithPageable(@Param("consumerNo") String consumerNo, Pageable pageable);

    @Query("from #{#entityName} b where b.consumerNo= :consumerNo order by to_date(b.billMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') ASC")
    public List<BillInterface> findByConsumerNoOrderByBillMonthAscendingWithPageable(@Param("consumerNo") String consumerNo, Pageable pageable);

    public long countByConsumerNo(String consumerNo);

    public List<BillInterface> findByConsumerNo(String consumerNo, Pageable pageable);

    @Query("select distinct(billMonth) from #{#entityName} b where b.locationCode =:locationCode")
    public List<String> findDistinctBillMonthByLocationCode(@Param("locationCode") String locationCode);

    @Query("select distinct(groupNo) from #{#entityName} b where b.locationCode =:locationCode and b.billMonth =:billMonth")
    public List<String> findDistinctGroupNoByLocationCodeAndBillMonth(@Param("locationCode") String locationCode, @Param("billMonth") String billMonth);

    public List<BillInterface> findByBillMonthAndGroupNoAndDeleted(String billMonth, String groupNo, boolean deleted);

    public List<BillInterface> findByConsumerNoAndBillMonthOrderByIdAsc(String consumerNo, String billMonth);
}
