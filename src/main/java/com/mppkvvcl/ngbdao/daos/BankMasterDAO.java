package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbinterface.interfaces.BankMasterInterface;
import com.mppkvvcl.ngbdao.interfaces.BankMasterDAOInterface;
import com.mppkvvcl.ngbdao.repositories.BankMasterRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
@Service
public class BankMasterDAO implements BankMasterDAOInterface<BankMasterInterface> {

    /**
     * Requesting spring to get singleton  BankMasterRepository object.
     */
    @Autowired
    private BankMasterRepository bankMasterRepository;


    /**
     * Getting whole logger object from global resources for current class.
     */
    Logger logger = GlobalResources.getLogger(BankMasterDAO.class);

    @Override
    public BankMasterInterface get(BankMasterInterface bankMaster) {
        final String methodName = "get() : ";
        logger.info(methodName + "is called");
        BankMasterInterface existingBankMaster = null;
        if(bankMaster != null){
            existingBankMaster = bankMasterRepository.findOne(bankMaster.getId());
        }
        return existingBankMaster;
    }

    @Override
    public BankMasterInterface add(BankMasterInterface bankMaster) {
        final String methodName = "add() : ";
        logger.info(methodName + "is called");
        BankMasterInterface insertedBankMaster = null;
        if(bankMaster != null){
            insertedBankMaster = bankMasterRepository.save(bankMaster);
        }
        return insertedBankMaster;
    }

    @Override
    public BankMasterInterface update(BankMasterInterface bankMaster) {
        final String methodName = "update() : ";
        logger.info(methodName + "is called");
        BankMasterInterface updatedBankMaster = null;
        if(bankMaster != null){
            updatedBankMaster = bankMasterRepository.save(bankMaster);
        }
        return updatedBankMaster;
    }

    @Override
    public BankMasterInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "is called");
        BankMasterInterface bankMaster = null;
        bankMaster = bankMasterRepository.findOne(id);
        return bankMaster;
    }

    @Override
    public List<? extends BankMasterInterface> getAll() {
        final String methodName = "getAll() : ";
        logger.info(methodName + "is called");
        List<? extends BankMasterInterface> bankMasters = null;
        bankMasters = bankMasterRepository.findAll();
        return bankMasters;
    }

}
