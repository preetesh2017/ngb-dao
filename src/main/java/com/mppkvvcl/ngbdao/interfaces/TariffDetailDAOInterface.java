package com.mppkvvcl.ngbdao.interfaces;

import com.mppkvvcl.ngbinterface.interfaces.TariffDetailInterface;

import java.util.Date;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface TariffDetailDAOInterface<T> extends DAOInterface<T>{
    public List<TariffDetailInterface> getByConsumerNoOrderByIdDesc(String consumerNo);
    public T getTopByConsumerNoOrderByIdDesc(String consumerNo);
    public List<T> getByConsumerNoAndEffectiveStartDateAndEffectiveEndDate(String consumerNo, Date effectiveStartDate, Date effectiveEndDate);
}
