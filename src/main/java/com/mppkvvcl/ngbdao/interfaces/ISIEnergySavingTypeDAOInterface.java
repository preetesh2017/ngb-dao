package com.mppkvvcl.ngbdao.interfaces;
import com.mppkvvcl.ngbinterface.interfaces.ISIEnergySavingTypeInterface;

import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
public interface ISIEnergySavingTypeDAOInterface<T> extends DAOInterface<T> {
    public List<T> getByType(String type);
    public List<? extends ISIEnergySavingTypeInterface> getAll();
}
