package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.CircleDAOInterface;
import com.mppkvvcl.ngbdao.repositories.CircleRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.CircleInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by RUPALI on 9/15/2017.
 */
@Service
public class CircleDAO implements CircleDAOInterface<CircleInterface> {

    @Autowired
    private CircleRepository circleRepository;
    /**
     * Get logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(CircleDAO.class);

    @Override
    public CircleInterface add(CircleInterface circle) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        CircleInterface insertedCircle = null;
        if(circle != null){
            insertedCircle = circleRepository.save(circle);
        }
        return insertedCircle;
    }

    @Override
    public CircleInterface update(CircleInterface circle) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        CircleInterface updatedCircle = null;
        if(circle != null){
            updatedCircle = circleRepository.save(circle);
        }
        return updatedCircle;
    }

    @Override
    public CircleInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        CircleInterface circle = null;
        circle = circleRepository.findOne(id);
        return circle;
    }

    @Override
    public CircleInterface get(CircleInterface circle) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        CircleInterface existingCircle = null;
        if(circle != null){
            existingCircle = circleRepository.findOne(circle.getId());
        }
        return existingCircle;
    }

    @Override
    public List<? extends CircleInterface> getAll() {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        List<? extends CircleInterface> existingCircles = circleRepository.findAll();
        return existingCircles;

    }
}
