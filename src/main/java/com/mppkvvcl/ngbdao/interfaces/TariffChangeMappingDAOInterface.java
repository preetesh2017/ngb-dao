package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by vikas on 10/6/2017.
 */
public interface TariffChangeMappingDAOInterface<T> extends DAOInterface<T> {

    public List<T> getByTariffCategory(String tariffCategory);
}
