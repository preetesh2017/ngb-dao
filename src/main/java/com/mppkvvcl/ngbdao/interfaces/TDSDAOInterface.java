package com.mppkvvcl.ngbdao.interfaces;

public interface TDSDAOInterface<T> extends DAOInterface<T> {

    public T getBySecurityDepositInterestId(long securityDepositInterestId);

}
