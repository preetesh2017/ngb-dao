package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.EEDivisionMappingInterface;
import com.mppkvvcl.ngbentity.beans.EEDivisionMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EEDivisionMappingRepository extends JpaRepository<EEDivisionMapping,Long> {

    public EEDivisionMappingInterface findByUserDetailUsername(String username);

    public EEDivisionMappingInterface findByDivisionId(long divisionId);

    public EEDivisionMappingInterface save(EEDivisionMappingInterface eeDivisionMappingInterface);
}
