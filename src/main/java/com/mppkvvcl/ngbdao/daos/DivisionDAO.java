package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.DivisionDAOInterface;
import com.mppkvvcl.ngbdao.repositories.DivisionRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.DivisionInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class DivisionDAO implements DivisionDAOInterface<DivisionInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(DivisionDAO.class);

    @Autowired
    DivisionRepository divisionRepository = null;

    @Override
    public DivisionInterface add(DivisionInterface division) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        DivisionInterface insertedDivision = null;
        if (division != null){
            insertedDivision = divisionRepository.save(division);
        }
        return insertedDivision;
    }

    @Override
    public DivisionInterface update(DivisionInterface division) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        DivisionInterface updatedDivision = null;
        if (division != null){
            updatedDivision = divisionRepository.save(division);
        }
        return updatedDivision;
    }

    @Override
    public DivisionInterface get(DivisionInterface division) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        DivisionInterface existingDivision = null;
        if (division != null){
            existingDivision = divisionRepository.findOne(division.getId());
        }
        return existingDivision;
    }

    @Override
    public DivisionInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        DivisionInterface existingDivision = null;
        if(id != 0){
            existingDivision = divisionRepository.findOne(id);
        }
        return existingDivision;
    }

    @Override
    public List<DivisionInterface> getByCircleId(long circleId) {
        final String methodName = "getByCircleId() : ";
        logger.info(methodName + "called");
        return divisionRepository.findByCircleId(circleId);
    }

    @Override
    public List<? extends DivisionInterface> getAll() {
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        return divisionRepository.findAll();
    }
}
