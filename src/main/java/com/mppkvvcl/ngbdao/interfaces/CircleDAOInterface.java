package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

/**
 * Created by RUPALI on 9/15/2017.
 */
public interface CircleDAOInterface<T> extends DAOInterface<T> {
    public List<? extends T> getAll();
}
