package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
public interface ConnectionTypeDAOInterface<T> extends DAOInterface<T> {
    List<? extends T> getAll();
}
