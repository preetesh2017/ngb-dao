package com.mppkvvcl.ngbdao.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by NITISH on 25-05-2017.
 */
public abstract class GlobalResources {

    public static Logger getLogger(Class className) {
        return LoggerFactory.getLogger(className);
    }

    public static final String BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB = "MON-YYYY";

    public static String getNextMonth(String date)  {

        String nextMonth = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(date));
            cal.add(Calendar.MONTH, 1);
            nextMonth = sdf.format(cal.getTime());

        } catch (Exception e) {
            e.getStackTrace();
        }
        return nextMonth.toString().toUpperCase();
    }

    public static String getPreviousMonth(String date) {
        String previousMonth = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(date));
            cal.add(Calendar.MONTH, -1);
            previousMonth = sdf.format(cal.getTime());

        } catch (Exception e) {
            e.getStackTrace();
        }
        return previousMonth.toString().toUpperCase();
    }


    public static String addMonth(String date, int month) throws ParseException {
        String newMonth = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(date));
            cal.add(Calendar.MONTH, month); //minus number would decrement the month
            newMonth = sdf.format(cal.getTime());

        } catch (Exception e) {
            e.getStackTrace();
        }
        return newMonth;
    }

    public static Date getCurrentDate() {
        return Calendar.getInstance().getTime();
    }

    /**
     * Get a diff between two dates
     *
     * @param date1 the oldest date
     * @param date2 the newest date
     * @return the diff value, in the days
     */
    public static long getDateDiffInDays(Date date1, Date date2) {
        long diffInDays = ((date2.getTime() - date1.getTime())
                / (1000 * 60 * 60 * 24));
        return diffInDays;
    }

    /**
     * To check given date (dateToCheck) is between Start Date and End Date
     * @param startDate
     * @param endDate
     * @param dateToCheck
     * @return
     */
    public static boolean checkDateBetween(Date startDate, Date endDate, Date dateToCheck) {
        if(startDate != null && endDate != null && dateToCheck != null){
            return dateToCheck.compareTo(startDate) * endDate.compareTo(dateToCheck) >= 0;
        }
        return false;
    }

    /**
     *  To check given date (readingDate) is between Previous Date and CurrentDate(Server Date)
     * @param previousDate
     * @param readingDate
     * @return
     */
    public static boolean checkDateBetweenPreviousDateAndCurrentDate(Date previousDate, Date readingDate){
        Date currentDate = getCurrentDate();
        if(previousDate != null && readingDate != null){
           return readingDate.compareTo(previousDate) * currentDate.compareTo(readingDate) >= 0;
        }
        return false;
    }

    public static Date getDateFromString(String DateInStringFormat) {
       Date dateInDateFormat = null ;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            dateInDateFormat = new Date();
            dateInDateFormat = sdf.parse(DateInStringFormat);

        } catch (Exception e) {
            e.getStackTrace();
        }
        return dateInDateFormat;
    }

    public static Date addDaysInDate(int days,Date date ){
        Date updatedDate = null;
        //Changed the code , where check of postive days was being done earlier code
        if(date != null){
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
             cal.add(Calendar.DATE, days);
            updatedDate = cal.getTime();
        }
        return updatedDate;
    }

    /**
     *  compare given two given bill month , returns integer  (0 if same both bill months are same)/ (-1, if first bill month< 2nd bill month)/(1, if first bill month>2nd bill month),
     * @param month1
     * @param month2
     * @return
     */
    public static int compareMonth(String month1, String month2) throws ParseException {
        int comparisionValue = 0;
        if(month1 != null && month2 != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            cal1.setTime(sdf.parse(month1));
            cal2.setTime(sdf.parse(month2));
            comparisionValue = cal1.compareTo(cal2);
        }
        return comparisionValue;
    }

}
