package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
public interface PurposeSubCategoryMappingDAOInterface<T> extends  DAOInterface<T> {
    public T getByPurposeOfInstallationIdAndSubcategoryCode(long purposeOfInstallationId,long subcategoryCode);
    public List<T> getByPurposeOfInstallationId(long purposeOfInstallationId);
    public List<T> getBySubcategoryCode(long subcategoryCode);
}
