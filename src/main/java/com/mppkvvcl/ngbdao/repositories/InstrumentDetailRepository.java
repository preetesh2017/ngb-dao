package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.InstrumentDetailInterface;
import com.mppkvvcl.ngbentity.beans.InstrumentDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ANSHIKA on 14-07-2017.
 */
@Repository
public interface InstrumentDetailRepository extends JpaRepository<InstrumentDetail, Long> {
    public List<InstrumentDetailInterface> findByDishonoured(boolean dishonoured);
    public InstrumentDetailInterface findByPayModeAndBankNameAndInstrumentNo(String payMode, String bankName, String instrumentNo);
    public InstrumentDetailInterface save(InstrumentDetailInterface instrumentDetailInterface);

    public List<InstrumentDetailInterface> findByInstrumentNo(String instrumentNo);
}