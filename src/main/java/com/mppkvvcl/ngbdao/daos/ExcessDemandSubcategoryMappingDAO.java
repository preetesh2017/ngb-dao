package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ExcessDemandSubcategoryMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ExcessDemandSubcategoryMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ExcessDemandSubcategoryMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExcessDemandSubcategoryMappingDAO implements ExcessDemandSubcategoryMappingDAOInterface<ExcessDemandSubcategoryMappingInterface> {

    private Logger logger = GlobalResources.getLogger(ExcessDemandSubcategoryMappingDAO.class);

    @Autowired
    private ExcessDemandSubcategoryMappingRepository excessDemandSubcategoryMappingRepository;

    @Override
    public List<ExcessDemandSubcategoryMappingInterface> getByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode) {
        final String methodName = "getByTariffIdAndSubcategoryCode() : ";
        logger.info(methodName + "called");
        List<ExcessDemandSubcategoryMappingInterface> excessDemandSubcategoryMappingInterfaces = excessDemandSubcategoryMappingRepository.findByTariffIdAndSubcategoryCode(tariffId,subcategoryCode);
        return excessDemandSubcategoryMappingInterfaces;
    }

    @Override
    public ExcessDemandSubcategoryMappingInterface add(ExcessDemandSubcategoryMappingInterface excessDemandSubcategoryMappingInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ExcessDemandSubcategoryMappingInterface addedExcessDemandSubcategoryMappingInterface = null;
        if(excessDemandSubcategoryMappingInterface != null){
            addedExcessDemandSubcategoryMappingInterface = excessDemandSubcategoryMappingRepository.save(excessDemandSubcategoryMappingInterface);
        }
        return addedExcessDemandSubcategoryMappingInterface;
    }

    @Override
    public ExcessDemandSubcategoryMappingInterface update(ExcessDemandSubcategoryMappingInterface excessDemandSubcategoryMappingInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ExcessDemandSubcategoryMappingInterface updatedExcessDemandSubcategoryMappingInterface = null;
        if(excessDemandSubcategoryMappingInterface != null){
            updatedExcessDemandSubcategoryMappingInterface = excessDemandSubcategoryMappingRepository.save(excessDemandSubcategoryMappingInterface);
        }
        return updatedExcessDemandSubcategoryMappingInterface;
    }

    @Override
    public ExcessDemandSubcategoryMappingInterface get(ExcessDemandSubcategoryMappingInterface excessDemandSubcategoryMappingInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ExcessDemandSubcategoryMappingInterface fetchedExcessDemandSubcategoryMappingInterface = null;
        if(excessDemandSubcategoryMappingInterface != null){
            fetchedExcessDemandSubcategoryMappingInterface = excessDemandSubcategoryMappingRepository.findOne(excessDemandSubcategoryMappingInterface.getId());
        }
        return fetchedExcessDemandSubcategoryMappingInterface;
    }

    @Override
    public ExcessDemandSubcategoryMappingInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ExcessDemandSubcategoryMappingInterface excessDemandSubcategoryMappingInterface = excessDemandSubcategoryMappingRepository.findOne(id);
        return excessDemandSubcategoryMappingInterface;
    }
}
