package com.mppkvvcl.ngbdao.interfaces;
import java.util.Date;

public interface LocalHolidayDAOInterface<T> extends DAOInterface<T> {
    public T getByLocationCodeAndDate(String locationCode, Date date);
}
