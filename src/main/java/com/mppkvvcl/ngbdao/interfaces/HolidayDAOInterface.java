package com.mppkvvcl.ngbdao.interfaces;
import java.util.Date;

/**
 * Created by PREETESH on 10/4/2017.
 */
public interface HolidayDAOInterface <T> extends DAOInterface<T>{
    public T getByDate(Date date);
}
