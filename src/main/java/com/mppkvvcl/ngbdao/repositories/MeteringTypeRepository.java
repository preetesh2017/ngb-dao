package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.MeteringTypeInterface;
import com.mppkvvcl.ngbentity.beans.MeteringType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by SHIVANSHU on 18-07-2017.
 */
@Repository
public interface MeteringTypeRepository extends JpaRepository<MeteringType , Long>{
    public MeteringType save(MeteringTypeInterface meteringTypeInterface);
}
