package com.mppkvvcl.ngbdao.interfaces;

import java.math.BigDecimal;

/**
 * Created by ANSHIKA on 15-09-2017.
 */
public interface AdjustmentRangeDAOInterface<T> extends DAOInterface<T> {

    public T getByCodeAndAmount(int code, BigDecimal amount);
}
