package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.PremiseTypeInterface;
import com.mppkvvcl.ngbentity.beans.PremiseType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by SHIVANSHU on 18-07-2017.
 */
@Repository
public interface PremiseTypeRepository extends JpaRepository<PremiseType , Long>{
    public PremiseTypeInterface save(PremiseTypeInterface premiseTypeInterface);
}
