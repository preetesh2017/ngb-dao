package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.OnlinePaymentRebateDAOInterface;
import com.mppkvvcl.ngbdao.repositories.OnlinePaymentRebateRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.OnlinePaymentRebateInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;

@Service
public class OnlinePaymentRebateDAO implements OnlinePaymentRebateDAOInterface<OnlinePaymentRebateInterface> {

    private Logger logger = GlobalResources.getLogger(OnlinePaymentRebateDAO.class);

    @Autowired
    private OnlinePaymentRebateRepository onlinePaymentRebateRepository;

    @Override
    public OnlinePaymentRebateInterface getByDate(Date date) {
        final String methodName = "getByDate() : ";
        logger.info(methodName + "called");
        OnlinePaymentRebateInterface onlinePaymentRebateInterface = null;
        if(date != null){
            onlinePaymentRebateInterface = onlinePaymentRebateRepository.findByDate(date);
        }
        return onlinePaymentRebateInterface;
    }

    @Override
    public OnlinePaymentRebateInterface add(OnlinePaymentRebateInterface onlinePaymentRebateInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        OnlinePaymentRebateInterface addedOnlinePaymentRebateInterface = null;
        if(onlinePaymentRebateInterface != null){
            addedOnlinePaymentRebateInterface = onlinePaymentRebateRepository.save(onlinePaymentRebateInterface);
        }
        return addedOnlinePaymentRebateInterface;
    }

    @Override
    public OnlinePaymentRebateInterface update(OnlinePaymentRebateInterface onlinePaymentRebateInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        OnlinePaymentRebateInterface updatedOnlinePaymentRebateInterface = null;
        if(onlinePaymentRebateInterface != null){
            updatedOnlinePaymentRebateInterface = onlinePaymentRebateRepository.save(onlinePaymentRebateInterface);
        }
        return updatedOnlinePaymentRebateInterface;
    }

    @Override
    public OnlinePaymentRebateInterface get(OnlinePaymentRebateInterface onlinePaymentRebateInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        OnlinePaymentRebateInterface fetchedOnlinePaymentRebateInterface = null;
        if(onlinePaymentRebateInterface != null){
            fetchedOnlinePaymentRebateInterface = onlinePaymentRebateRepository.findOne(onlinePaymentRebateInterface.getId());
        }
        return fetchedOnlinePaymentRebateInterface;
    }

    @Override
    public OnlinePaymentRebateInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        OnlinePaymentRebateInterface fetchedOnlinePaymentRebateInterface = null;
        fetchedOnlinePaymentRebateInterface = onlinePaymentRebateRepository.findOne(id);
        return fetchedOnlinePaymentRebateInterface;
    }
}
