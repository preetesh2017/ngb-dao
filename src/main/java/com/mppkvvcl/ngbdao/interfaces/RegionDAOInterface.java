package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface RegionDAOInterface<T> extends DAOInterface<T>{
    public List<? extends T> getAll();
}
