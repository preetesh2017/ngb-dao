package com.mppkvvcl.ngbdao.interfaces;

import java.util.Date;
import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
public interface ConsumerConnectionInformationHistoryDAOInterface<T> extends DAOInterface<T> {
    public List<T> getByConsumerNo(String consumerNo);
    public List<T> getByConsumerNoAndPropertyName(String consumerNo, String propertyName);
    public List<T> getByConsumerNoAndPropertyNameAndEndDateBetween(String consumerNo, String propertyName , Date startDate, Date endDate);
    public List<T> getByConsumerNoAndEndBillMonth(String consumerNo,String endMonth);
    public List<T> getByConsumerNoAndPropertyNameAndEndBillMonth(String consumerNo,String propertyName ,String endMonth);

}
