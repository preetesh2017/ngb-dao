package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.CTRMasterDAOInterface;
import com.mppkvvcl.ngbdao.repositories.CTRMasterRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.CTRMasterInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
@Service
public class CTRMasterDAO implements CTRMasterDAOInterface<CTRMasterInterface> {

    private Logger logger = GlobalResources.getLogger(CTRMasterDAO.class);

    @Autowired
    private CTRMasterRepository ctrMasterRepository;

    @Override
    public CTRMasterInterface add(CTRMasterInterface ctrMaster) {
        final String methodName  = "add() :  ";
        logger.info(methodName+"clicked");
        CTRMasterInterface insertedCtrMaster =  null;
        if (ctrMaster != null){
            insertedCtrMaster = ctrMasterRepository.save(ctrMaster);
        }
        return insertedCtrMaster;
    }

    @Override
    public CTRMasterInterface update(CTRMasterInterface ctrMaster) {
        final String methodName  = "update() :  ";
        logger.info(methodName+"clicked");
        CTRMasterInterface updatedCtrMaster =  null;
        if (ctrMaster != null){
            updatedCtrMaster = ctrMasterRepository.save(ctrMaster);
        }
        return updatedCtrMaster;
    }

    @Override
    public CTRMasterInterface get(CTRMasterInterface ctrMaster) {
        final String methodName  = "get() :  ";
        logger.info(methodName+"clicked");
        CTRMasterInterface getCtrMaster =  null;
        if (ctrMaster != null){
            getCtrMaster = ctrMasterRepository.findOne(ctrMaster.getId());
        }
        return getCtrMaster;
    }

    @Override
    public CTRMasterInterface getById(long id) {
        final String methodName  = "getById() :  ";
        logger.info(methodName+"clicked");
        CTRMasterInterface getCtrMaster =  null;
        if (id > 0){
            getCtrMaster = ctrMasterRepository.findOne(id);
        }
        return getCtrMaster;
    }

    @Override
    public List<CTRMasterInterface> getBySerialNo(String serialNo) {
        final String methdoName = "getBySerialNo() :  ";
        logger.info(methdoName+"clicked");
        List<CTRMasterInterface> ctrMasters = null;
        if (serialNo != null){
            ctrMasters = ctrMasterRepository.findBySerialNo(serialNo);
        }
        return ctrMasters;
    }

    @Override
    public List<CTRMasterInterface> getByMake(String make) {
        final String methodName = "getBYMake() : ";
        logger.info(methodName+"clicked");
        List<CTRMasterInterface> ctrMasters = null;
        if (make != null){
            ctrMasters = ctrMasterRepository.findByMake(make);
        }
        return ctrMasters;
    }

    @Override
    public CTRMasterInterface getByIdentifier(String identifier) {
        final String methodName = "getByIdentifier() :  ";
        logger.info(methodName+"clicked");
        CTRMasterInterface ctrMaster = null;
        if (identifier != null){
            ctrMaster = ctrMasterRepository.findByIdentifier(identifier);
        }
        return ctrMaster;
    }

    @Override
    public CTRMasterInterface getBySerialNoAndMake(String serialNo, String make) {
        final String methodName = "getBySerialNoAndMake";
        logger.info(methodName+"clicked");
        CTRMasterInterface ctrMaster = null;
        if (serialNo != null && make != null){
            ctrMaster = ctrMasterRepository.findBySerialNoAndMake(serialNo , make);
        }
        return ctrMaster;
    }
}
