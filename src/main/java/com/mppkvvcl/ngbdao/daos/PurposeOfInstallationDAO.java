package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.PurposeOfInstallationDAOInterface;
import com.mppkvvcl.ngbdao.repositories.PurposeOfInstallationRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PurposeOfInstallationInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
@Service
public class PurposeOfInstallationDAO implements PurposeOfInstallationDAOInterface<PurposeOfInstallationInterface> {
    @Autowired
    PurposeOfInstallationRepository purposeOfInstallationRepository;
    /**
     *
     */
    Logger logger = GlobalResources.getLogger(PurposeOfInstallationDAO.class);

    @Override
    public List<PurposeOfInstallationInterface> getByTariffCode(String tariffCode) {
        final String methodName = "getByTariffCode() : ";
        logger.info(methodName + "called");
        List<PurposeOfInstallationInterface> purposeOfInstallations = null;
        if(tariffCode != null){
            purposeOfInstallations = purposeOfInstallationRepository.findByTariffCode(tariffCode);
        }
        return purposeOfInstallations;
    }

    @Override
    public PurposeOfInstallationInterface getByPurposeOfInstallation(String purposeOfInstallation) {
        final String methodName = "getByPurposeOfInstallation() : ";
        logger.info(methodName + "called");
        PurposeOfInstallationInterface existingPurposeOfInstallation = null;
        if(purposeOfInstallation != null){
            existingPurposeOfInstallation = purposeOfInstallationRepository.findByPurposeOfInstallation(purposeOfInstallation);
        }
        return existingPurposeOfInstallation;
    }

    @Override
    public PurposeOfInstallationInterface add(PurposeOfInstallationInterface purposeOfInstallation) {
        final String methodName = "add() : ";
        logger.info(methodName + " called");
        PurposeOfInstallationInterface insertedPurposeOfInstallation = null;
        if(purposeOfInstallation != null){
            insertedPurposeOfInstallation = purposeOfInstallationRepository.save(purposeOfInstallation);
        }
        return insertedPurposeOfInstallation;
    }

    @Override
    public PurposeOfInstallationInterface get(PurposeOfInstallationInterface purposeOfInstallation) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        PurposeOfInstallationInterface existingPurposeOfInstallation = null;
        if(purposeOfInstallation != null){
            existingPurposeOfInstallation = purposeOfInstallationRepository.findOne(purposeOfInstallation.getId());
        }
        return existingPurposeOfInstallation;
    }

    @Override
    public PurposeOfInstallationInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + " called");
        PurposeOfInstallationInterface purposeOfInstallation = null;
        purposeOfInstallation = purposeOfInstallationRepository.findOne(id);
        return purposeOfInstallation;
    }

    @Override
    public PurposeOfInstallationInterface update(PurposeOfInstallationInterface purposeOfInstallation) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        PurposeOfInstallationInterface updatedPurposeOfInstallation = null;
        if(purposeOfInstallation != null){
            updatedPurposeOfInstallation = purposeOfInstallationRepository.save(purposeOfInstallation);
        }
        return updatedPurposeOfInstallation;
    }
}
