package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ReadingDiaryNoInterface;
import com.mppkvvcl.ngbentity.beans.ReadingDiaryNo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReadingDiaryNoRepository extends JpaRepository<ReadingDiaryNo,Long>{

    public List<ReadingDiaryNoInterface> findByGroupNo(String groupNo);

    public ReadingDiaryNoInterface save(ReadingDiaryNoInterface readingDiaryNo);

    public ReadingDiaryNoInterface findByGroupNoAndReadingDiaryNo(String groupNo, String readingDiaryNo);
}
