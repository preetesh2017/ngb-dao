package com.mppkvvcl.ngbdao.interfaces;

public interface WeldingSurchargeConfigurationDAOInterface<T> extends DAOInterface<T> {
    public T getByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode);
}
