package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface GovernmentTypeDAOInterface<T> extends DAOInterface<T>{

    public T getByType(String type);

    public List<? extends T> getAll();

}
