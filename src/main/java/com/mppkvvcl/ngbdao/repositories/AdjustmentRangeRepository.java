package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.AdjustmentRangeInterface;
import com.mppkvvcl.ngbentity.beans.AdjustmentRange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
 * Created by ANKIT on 18-08-2017.
 */
@Repository
public interface AdjustmentRangeRepository extends JpaRepository<AdjustmentRange,Long> {


    @Query("from AdjustmentRange a where a.code = :code and a.start<= :amount and a.end >= :amount")
    public AdjustmentRangeInterface findByCodeAndAmount(@Param("code") int code, @Param("amount") BigDecimal amount);

    public AdjustmentRangeInterface save(AdjustmentRangeInterface adjustmentRangeInterface);
}
