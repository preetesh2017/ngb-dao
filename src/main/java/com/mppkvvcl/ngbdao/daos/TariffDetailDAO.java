package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.TariffDetailDAOInterface;
import com.mppkvvcl.ngbdao.repositories.TariffDetailRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.TariffDetailInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class TariffDetailDAO implements TariffDetailDAOInterface<TariffDetailInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(TariffDetailDAO.class);

    @Autowired
    private TariffDetailRepository tariffDetailRepository = null;
    
    @Override
    public TariffDetailInterface add(TariffDetailInterface tariffDetail) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        TariffDetailInterface insertedTariffDetail = null;
        if (tariffDetail != null){
            insertedTariffDetail = tariffDetailRepository.save(tariffDetail);
        }
        return insertedTariffDetail;    }

    @Override
    public TariffDetailInterface update(TariffDetailInterface tariffDetail) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        TariffDetailInterface updatedTariffDetail = null;
        if (tariffDetail != null){
            updatedTariffDetail = tariffDetailRepository.save(tariffDetail);
        }
        return updatedTariffDetail;    }

    @Override
    public TariffDetailInterface get(TariffDetailInterface tariffDetail) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        TariffDetailInterface existingTariffDetail = null;
        if (tariffDetail != null){
            existingTariffDetail = tariffDetailRepository.findOne(tariffDetail.getId());
        }
        return existingTariffDetail;
    }

    @Override
    public TariffDetailInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        TariffDetailInterface existingTariffDetail = null;
        if(id != 0){
            existingTariffDetail = tariffDetailRepository.findOne(id);
        }
        return existingTariffDetail;
    }

    @Override
    public TariffDetailInterface getTopByConsumerNoOrderByIdDesc(String consumerNo) {
        final String methodName  = "getTopByConsumerNoOrderByIdDesc()  : ";
        logger.info(methodName+"called");
        TariffDetailInterface tariffDetail = null;
        if (consumerNo != null){
            tariffDetail = tariffDetailRepository.findTopByConsumerNoOrderByIdDesc(consumerNo);
        }
        return tariffDetail;
    }

    @Override
    public List<TariffDetailInterface> getByConsumerNoAndEffectiveStartDateAndEffectiveEndDate(String consumerNo, Date effectiveStartDate, Date effectiveEndDate) {
        final String methodName  = "getByConsumerNoAndEffectiveStartDateAndEffectiveEndDate()  : ";
        logger.info(methodName+"called");
        List<TariffDetailInterface> tariffDetailInterfaces = null;
        if(consumerNo != null && effectiveStartDate != null && effectiveEndDate != null){
            tariffDetailInterfaces = tariffDetailRepository.findByConsumerNoAndEffectiveStartDateAndEffectiveEndDate(consumerNo,effectiveStartDate,effectiveEndDate);
        }
        return tariffDetailInterfaces;
    }

    @Override
    public List<TariffDetailInterface> getByConsumerNoOrderByIdDesc(String consumerNo) {
        final String methodName  = "getTopByConsumerNoOrderByIdDesc()  : ";
        logger.info(methodName+"called");
        List<TariffDetailInterface> tariffDetails = null;
        if (consumerNo != null){
            tariffDetails = tariffDetailRepository.findByConsumerNoOrderByIdDesc(consumerNo);
        }
        return tariffDetails;
    }
}
